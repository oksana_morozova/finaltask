-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: trainingcenter
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idAdress_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'Akademica Kuprevicha str, 11'),(2,'Independence avenue, 4'),(3,'Filimonava str, 25'),(4,'Gikalo str, 9'),(5,'Zhykov avenue, 29');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idCity_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Minsk'),(2,'Gomel'),(3,'Brest'),(4,'Kiev'),(5,'Saint-Petersburg'),(6,'Saratov'),(7,'Nursultan');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trainingId` int(11) NOT NULL,
  `addressId` int(11) NOT NULL,
  `teacherId` int(11) NOT NULL,
  PRIMARY KEY (`id`,`trainingId`,`addressId`,`teacherId`),
  UNIQUE KEY `idGroup_UNIQUE` (`id`),
  KEY `trainingId_fk_idx` (`trainingId`),
  KEY `addressId_fk_idx` (`addressId`),
  KEY `teacherId_fk_idx` (`teacherId`),
  CONSTRAINT `addressId_fk` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `teacherId_fk` FOREIGN KEY (`teacherId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trainingId_fk` FOREIGN KEY (`trainingId`) REFERENCES `training` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,1,1,18);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentgroup`
--

DROP TABLE IF EXISTS `studentgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  PRIMARY KEY (`id`,`groupId`),
  UNIQUE KEY `idStudentGroup_UNIQUE` (`id`),
  KEY `studentIdGr_fk_idx` (`studentId`),
  KEY `groupId_fk_idx` (`groupId`),
  CONSTRAINT `groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `studentIdGr_fk` FOREIGN KEY (`studentId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentgroup`
--

LOCK TABLES `studentgroup` WRITE;
/*!40000 ALTER TABLE `studentgroup` DISABLE KEYS */;
INSERT INTO `studentgroup` VALUES (8,2,1),(1,3,1),(2,4,1),(3,5,1),(9,6,1),(10,12,1),(4,13,1),(5,14,1),(6,15,1),(7,17,1);
/*!40000 ALTER TABLE `studentgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studenttask`
--

DROP TABLE IF EXISTS `studenttask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studenttask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentId` int(11) NOT NULL,
  `taskId` int(11) NOT NULL,
  `taskStatus` enum('IN_PROCESS','DONE','CHECKED') NOT NULL,
  `mark` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`taskId`),
  UNIQUE KEY `idStudentTask_UNIQUE` (`id`),
  KEY `studentId_fk_idx` (`studentId`),
  KEY `taskId_fk_idx` (`taskId`),
  CONSTRAINT `studentIdTsk_fk` FOREIGN KEY (`studentId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `taskId_fk` FOREIGN KEY (`taskId`) REFERENCES `task` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studenttask`
--

LOCK TABLES `studenttask` WRITE;
/*!40000 ALTER TABLE `studenttask` DISABLE KEYS */;
INSERT INTO `studenttask` VALUES (1,2,1,'DONE',NULL),(2,2,2,'DONE',NULL),(3,3,1,'IN_PROCESS',NULL),(4,3,2,'IN_PROCESS',NULL),(5,4,1,'IN_PROCESS',NULL),(6,4,2,'CHECKED',9),(7,5,1,'CHECKED',0),(8,5,2,'IN_PROCESS',NULL),(9,6,1,'DONE',NULL),(10,6,2,'IN_PROCESS',NULL),(11,12,1,'CHECKED',5),(12,12,2,'IN_PROCESS',NULL),(13,13,1,'IN_PROCESS',NULL),(14,13,2,'CHECKED',9),(15,14,1,'IN_PROCESS',NULL),(16,14,2,'CHECKED',6),(17,15,1,'IN_PROCESS',NULL),(18,15,2,'CHECKED',8),(19,17,1,'IN_PROCESS',NULL),(20,17,2,'DONE',NULL),(66,3,8,'IN_PROCESS',NULL),(67,4,8,'IN_PROCESS',NULL),(68,5,8,'IN_PROCESS',NULL),(69,13,8,'IN_PROCESS',NULL),(70,14,8,'IN_PROCESS',NULL),(71,15,8,'IN_PROCESS',NULL),(72,17,8,'IN_PROCESS',NULL),(73,2,8,'IN_PROCESS',NULL),(74,6,8,'IN_PROCESS',NULL),(75,12,8,'IN_PROCESS',NULL);
/*!40000 ALTER TABLE `studenttask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (1,'Java Fundamentals','Create an application that parses text from a file and allows you to perform three different operations on the text.'),(2,'Java Threads','Develop a multithreaded application that uses shared resources. Any entity that wants to access a shared resource must be a thread. The application must implement the functionality defined by the individual task.'),(3,'HTML\\CSS Fundamentals','To develop the main or the most functional page of your project (only HTML and CSS, without frameworks). Pay attention to the content. It should be enough to understand the purpose and functionality of the page.'),(4,'MySql Fundamentals','Create a database for your subject area. Present the database model in the format .mdb .Fill tables with records in the volume sufficient for demonstration of the subsequent requests (on 2-3 records in tables it is not enough). \nSend scripts for creating database objects and filling them with records. Send a separate file with screenshots of query results \nselect * from table_name for all tables.'),(8,'Java HTML','Create the main page of your final project or the most functional page.');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `training`
--

DROP TABLE IF EXISTS `training`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `trainingStatus` enum('CLOSED_RECRUITMENT','FORTHCOMING_RECRUITMENT','UNDER_RECRUITMENT') NOT NULL,
  `duration` varchar(45) NOT NULL,
  `numberOfStudents` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `startDate` varchar(45) DEFAULT NULL,
  `contactEmail` varchar(45) NOT NULL,
  `picturePath` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`cityId`),
  UNIQUE KEY `idTraining_UNIQUE` (`id`),
  KEY `cityId_fk_idx` (`cityId`),
  CONSTRAINT `cityId_fk` FOREIGN KEY (`cityId`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `training`
--

LOCK TABLES `training` WRITE;
/*!40000 ALTER TABLE `training` DISABLE KEYS */;
INSERT INTO `training` VALUES (1,'Java Web','Java is one of the top three most popular programming languages and is intended to develop enterprise-level applications.','CLOSED_RECRUITMENT','10 weeks',10,1,'2019-May','anna.korzyk@training.com','java-web.png'),(2,'Phyton','Python is one of the easiest and most flexible programming languages, meant to improve development productivity and code readability.','UNDER_RECRUITMENT','4-5 weeks',8,1,'2019-September','anna.korzyk@training.com','phyton.png'),(3,'DevOps','DevOps is at the intersection of providing stable flow of product development, management of dynamic Cloud solutions and development acceleration.','UNDER_RECRUITMENT','4 weeks',8,1,'2019-September','anna.korzyk@training.com','devops.png'),(4,'Cloud & DevOps','DevOps is at the intersection of providing stable flow of product development, management of dynamic Cloud solutions and development acceleration.','FORTHCOMING_RECRUITMENT','4 weeks',8,2,'2019-November','viktoryia.ermolovich@training.com','cloud-devops.png'),(5,'.Net','.NET is a powerful Microsoft platform for developing applications of any level of complexity. It is flexible, easy to learn, and commonly used.','UNDER_RECRUITMENT','7 weeks',10,2,'2019-November','viktoryia.ermolovich@training.com','net.png'),(6,'JavaScript','JavaScript is one of the most popular and cross-platform programming languages, allowing to work with web based interfaces, as well as with server side and mobile clients.','FORTHCOMING_RECRUITMENT','5 weeks',10,3,'2019-December','elena.tatarinova@trainingl.com','front-end.png'),(7,'Front-End','JavaScript is one of the most popular and cross-platform programming languages, allowing to work with web based interfaces, as well as with server side and mobile clients.','FORTHCOMING_RECRUITMENT','9 weeks',10,5,'2019-December','nataliia.sidorova@training.com','front-end.png'),(8,'Software testing','Software Testing is a never-ending struggle for quality. It is the work of a software testing engineer that often makes out end user happiness.','FORTHCOMING_RECRUITMENT','7 weeks',10,4,'2019-December','vera.berdnikova@training.com','testing.png'),(9,'Java 0','Java is one of the top three most popular programming languages and is intended to develop enterprise-level applications.','FORTHCOMING_RECRUITMENT','6 weeks',10,1,'2019-December','anna.korzyk@training.com','java-zero.png');
/*!40000 ALTER TABLE `training` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `role` enum('STUDENT','TEACHER','ADMIN') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idUser_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'cc9f816a42431cf852cdc7a3fad42a6f65ffce24','Oksana','Morozova','oksana.morozova@training.com','ADMIN'),(2,'cc9f816a42431cf852cdc7a3fad42a6f65ffce24','Ivan','Matesha','ivan.matesha@gmail.com','STUDENT'),(3,'5542bffda800762369192a583cbf80eb5f4d7318','Nikita','Okynev','nikita.okynev@gmail.com','STUDENT'),(4,'576ac2a38b7412afdde5a798c16f0372c38d7eaa','Anna','Kovalionok','anna.kovalionak@gmail.com','STUDENT'),(5,'d91779eb6f0f188c5d12f133f197ebd5e5775b07','Alex','Kyrbako','alex.kyrbako@gmail.com','STUDENT'),(6,'53b088354dab83e4b09ca0beaebbe2c4159135af','Antos','Yurevich','antos.yurevich@gmail.com','STUDENT'),(7,'284f73b9ff94ed02d728579ce2fe00aaa88ef5bf','Mila','Meshkova','mila.meshkova@gmail.com','STUDENT'),(8,'f735dbf40991afa14332886f4966e5df7c285fd5','Ksysha','Nesterovich','ksysha.nesterovich@gmail.com','STUDENT'),(9,'96d2921e27cea6db75527e389e6d8eecb9c26c27','Daniil','Lyzo','daniil.lyzo@gmail.com','STUDENT'),(10,'7d776c17e0a863d08184752d717d17886d917e83','Sasha','Antilevskaya','sasha.antilevskaya@gmail.com','STUDENT'),(11,'28203f853df2e546d8472a3419a41240f1b3680d','Kirill','Sakun','kirill.sakun@gmail.com','STUDENT'),(12,'b507fbaf853528564e70d05ace2c33e9daa325c0','Pavel','Sobko','pavel.sobko@gmail.com','STUDENT'),(13,'d2484d1e797ef29ccfb87e8eb8249b4301f043f9','Dima','Dobrovolsky','dima.dobrovolsky@gmail.com','STUDENT'),(14,'cccbd0c453008efd1a11a67d68ea175f3ce4352e','Artiom','Gorbach','artiom.gorbach@gmail.com','STUDENT'),(15,'b5f0859878ee31ed71699779c06895c0ae79da62','Margo','Bychek','margo.bychek@gmai.com','STUDENT'),(16,'a71e33c47c7bb8a3583ed5c32bcac11c06523bc7','Valentin','Logvinov','valentin.logvinov@gmail.com','STUDENT'),(17,'ad5226aefa428b3082fa6957b94383fe6a7d33f5','Andrey','Kovaliov','andrey.kovaliov@gmail.com','STUDENT'),(18,'cc9f816a42431cf852cdc7a3fad42a6f65ffce24','Valery','Zhmishenko','valery.zhmishenko@training.com','TEACHER'),(19,'c13b8167993c91df54a9d4922267690a1a90ea17','Stas','Baretsky','stas.baretsky@training.com','TEACHER'),(20,'19810db7ed846b1117d16cc6b93ffe12b24c8308','Alexey','Danilov','alexey.danilov@training.com','TEACHER'),(21,'6616b663d0ec85ab9bed2c5f5947dc7978c9e6f9','Liza','Korzyk','liza.korzyk@training.com','TEACHER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-07 17:35:42
