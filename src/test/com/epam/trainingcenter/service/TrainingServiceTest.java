package com.epam.trainingcenter.service;

import com.epam.trainingcenter.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Tests class for testing training service behaviour.
 *
 * @author Oksana Morozova
 * @see TrainingService
 */
@RunWith(Parameterized.class)
public class TrainingServiceTest {
    private String name;
    private String description;
    private boolean isCorrect;

    private TrainingService service = new TrainingService();

    /**
     * Instantiates a new TrainingServiceTest.
     *
     * @param name        the training name.
     * @param description the training description.
     * @param isCorrect   the expected behaviour.
     */
    public TrainingServiceTest(String name, String description, boolean isCorrect) {
        this.name = name;
        this.description = description;
        this.isCorrect = isCorrect;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"Java Web", "Java is one of the top three most popular programming languages and is intended " +
                                "to develop enterprise-level applications.", true},
                        {"Test", "Test", false}
                }
        );
    }

    /**
     * Tests the check training for unique method behaviour.
     */
    @Test
    public void checkTrainingForUniqueTest() throws ServiceException {
        boolean actual = service.checkTrainingForUnique(name, description);

        assertEquals(isCorrect, actual);
    }
}