package com.epam.trainingcenter.service;

import com.epam.trainingcenter.exception.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Tests class for testing task service behaviour.
 *
 * @author Oksana Morozova
 * @see TaskService
 */
@RunWith(Parameterized.class)
public class TaskServiceTest {

    private String name;
    private String description;
    private boolean isCorrect;

    private TaskService service = new TaskService();

    /**
     * Instantiates a new TaskServiceTest.
     *
     * @param name        the task name.
     * @param description the task description.
     * @param isCorrect   the expected behaviour.
     */
    public TaskServiceTest(String name, String description, boolean isCorrect) {
        this.name = name;
        this.description = description;
        this.isCorrect = isCorrect;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"Java Fundamentals", "Create an application that parses text from a file and allows you to " +
                                "perform three different operations on the text.", true},
                        {"Test", "Test", false}
                }
        );
    }

    /**
     * Tests the check task for unique method behaviour.
     */
    @Test
    public void checkTaskForUniqueTest() throws ServiceException {
        boolean actual = service.checkTaskForUnique(name, description);

        assertEquals(isCorrect, actual);
    }
}