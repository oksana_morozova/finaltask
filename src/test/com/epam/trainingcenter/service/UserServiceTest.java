package com.epam.trainingcenter.service;

import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.util.PasswordEncoder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Tests class for testing user service behaviour.
 *
 * @author Oksana Morozova
 * @see UserService
 */
@RunWith(Parameterized.class)
public class UserServiceTest {
    private String name;
    private String surname;
    private String email;
    private String password;
    private Role role;
    private int id;
    private boolean isCorrect;

    private UserService service = new UserService();

    /**
     * Instantiates a new UserServiceTest.
     *
     * @param name      the user name.
     * @param surname   the user description.
     * @param email     the user email.
     * @param password  the user password.
     * @param role      the user role.
     * @param id        the user id.
     * @param isCorrect the expected behaviour.
     */
    public UserServiceTest(String name, String surname, String email, String password, Role role,
                           int id, boolean isCorrect) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.role = role;
        this.id = id;
        this.isCorrect = isCorrect;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"Liza", "Korzyk", "liza.korzyk@training.com", "Liza123", Role.TEACHER, 21, true},
                        {"Test", "Test", "test@training.com", "Test123", Role.TEACHER, 21, false}
                }
        );
    }

    /**
     * Tests the login method behaviour.
     */
    @Test
    public void loginTest() throws ServiceException {
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(PasswordEncoder.encode(password));
        user.setRole(role);
        user.setId(id);
        Optional<User> optionalUser;
        try {
            optionalUser = service.login(email, password);
            assertEquals(user, optionalUser.get());
        } catch (NoSuchElementException ex) {
            assertFalse(isCorrect);
        }
    }

    /**
     * Tests the check user login for unique method behaviour.
     */
    @Test
    public void checkUserLoginForUniqueTest() throws ServiceException {
        boolean actual = service.checkUserLoginForUnique(email);

        assertEquals(isCorrect, actual);
    }
}