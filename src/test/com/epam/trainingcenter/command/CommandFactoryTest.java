package com.epam.trainingcenter.command;

import com.epam.trainingcenter.command.admin.AddNewTeacherCommand;
import com.epam.trainingcenter.command.common.EmptyCommand;
import com.epam.trainingcenter.command.common.LoginCommand;
import com.epam.trainingcenter.command.student.ChangeTaskStatusCommand;
import com.epam.trainingcenter.command.teacher.SetMarkCommand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;

import java.util.Arrays;
import java.util.Collection;

import static com.epam.trainingcenter.command.Command.COMMAND_PARAMETER;
import static org.junit.Assert.*;

/**
 * Tests class for testing command factory behaviour.
 *
 * @author Oksana Morozova
 * @see CommandFactory
 * @see Command
 * @see HttpServletRequest
 */
@RunWith(Parameterized.class)
public class CommandFactoryTest extends Mockito {
    private String strCommand;
    private Command command;

    /**
     * Instantiates a new CommandFactoryTest.
     *
     * @param strCommand the command from user.
     * @param command    the expected command.
     */
    public CommandFactoryTest(String strCommand, Command command) {
        this.strCommand = strCommand;
        this.command = command;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"common_login", new LoginCommand()},
                        {"student_change_task_status", new ChangeTaskStatusCommand()},
                        {"teacher_set_mark", new SetMarkCommand()},
                        {"admin_add_new_teacher", new AddNewTeacherCommand()},
                        {null, new EmptyCommand()},
                        {"unknown_command", new EmptyCommand()}
                }
        );
    }

    /**
     * Tests the command method behaviour.
     */
    @Test
    public void defineCommandTest() {
        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getParameter(COMMAND_PARAMETER)).thenReturn(strCommand);

        CommandFactory factory = new CommandFactory();
        Class actual = factory.defineCommand(request).getClass();
        Class expected = command.getClass();

        assertEquals(expected, actual);
    }
}