package com.epam.trainingcenter.controller;

import com.epam.trainingcenter.command.Page;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Tests class for testing controller behaviour.
 *
 * @author Oksana Morozova
 * @see Controller
 * @see HttpServletRequest
 * @see HttpServletResponse
 * @see RequestDispatcher
 */
public class ControllerTest extends Mockito {

    /**
     * Tests the controller behaviour.
     */
    @Test
    public void ControllerEmptyCommandTest() throws ServletException, IOException {
        Controller controller = new Controller();

        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        RequestDispatcher dispatcher = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher(Page.MAIN_PAGE_PATH)).thenReturn(dispatcher);

        controller.doGet(request, response);

        verify(request, times(1)).getRequestDispatcher(Page.MAIN_PAGE_PATH);
        verify(request, never()).getSession();
        verify(dispatcher).forward(request, response);
    }
}