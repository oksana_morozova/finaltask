package com.epam.trainingcenter.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Tests class for user data validator behaviour.
 *
 * @author Oksana Morozova
 * @see UserDataValidator
 */
@RunWith(Parameterized.class)
public class UserDataValidatorTest {
    private String login;
    private String password;
    private String name;
    private String surname;
    private boolean isCorrect;

    private UserDataValidator validator = new UserDataValidator();

    /**
     * Instantiates a new UserDataValidatorTest.
     *
     * @param login     the login of the user.
     * @param password  the password of the user.
     * @param name      the name of the user.
     * @param surname   the surname of the user.
     * @param isCorrect the expected behaviour.
     */
    public UserDataValidatorTest(String login, String password, String name, String surname, boolean isCorrect) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.isCorrect = isCorrect;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"test@mail.com", "Test123", "Testtest", "Testtest", true},
                        {"Test", "Test", "Test", "Test", false},
                        {"test@mail.com", "Test", "Test", "Test", false},
                        {"test@mail.com", "Test123", "Test123", "Test", false},
                        {"test@mail.com", "Test123", "Test", "Test123", false},
                }
        );
    }

    /**
     * Tests the check data method behaviour.
     */
    @Test
    public void checkDataTest() {
        boolean actual = validator.checkData(login, password, name, surname);

        assertEquals(isCorrect, actual);
    }
}