package com.epam.trainingcenter.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Tests class for task data validator behaviour.
 *
 * @author Oksana Morozova
 * @see TaskDataValidator
 */
@RunWith(Parameterized.class)
public class TaskDataValidatorTest {
    private String mark;
    private String name;
    private String description;
    private boolean isCorrect;

    private TaskDataValidator validator = new TaskDataValidator();

    /**
     * Instantiates a new TaskDataValidatorTest.
     *
     * @param mark        the mark for the task.
     * @param name        the name of the task.
     * @param description the description of the task.
     */
    public TaskDataValidatorTest(String mark, String name, String description, boolean isCorrect) {
        this.mark = mark;
        this.name = name;
        this.description = description;
        this.isCorrect = isCorrect;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"7", "abcd", "abcd", true},
                        {"-1", null, "abcd", false},
                        {"1.5", "", "abcd", false},
                        {"100", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "abcd", false},
                        {null, null, null, false},
                }
        );
    }

    /**
     * Tests the check mark method behaviour.
     */
    @Test
    public void checkMarkTest() {
        boolean actual = validator.checkMark(mark);

        assertEquals(isCorrect, actual);
    }

    /**
     * Tests the check data method behaviour.
     */
    @Test
    public void checkDataTest() {
        boolean actual = validator.checkData(name, description);

        assertEquals(isCorrect, actual);
    }
}