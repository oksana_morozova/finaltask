package com.epam.trainingcenter.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Tests class for site scripting validator behaviour.
 *
 * @author Oksana Morozova
 * @see SiteScriptingValidator
 */
@RunWith(Parameterized.class)
public class SiteScriptingValidatorTest {
    private String toProtect;
    private String wasProtected;

    private SiteScriptingValidator validator = new SiteScriptingValidator();

    /**
     * Instantiates a new SiteScriptingValidatorTest.
     *
     * @param toProtect    the string needed to be protected.
     * @param wasProtected the expected behaviour.
     */
    public SiteScriptingValidatorTest(String toProtect, String wasProtected) {
        this.toProtect = toProtect;
        this.wasProtected = wasProtected;
    }

    /**
     * Scope of data for testing.
     */
    @Parameterized.Parameters
    public static Collection commands() {
        return Arrays.asList(new Object[][]{
                        {"<>", "&lt;&gt;"},
                        {"abcd", "abcd"},
                        {"ab<cd", "ab&lt;cd"},
                        {"ab>cd", "ab&gt;cd"}
                }
        );
    }

    /**
     * Tests the protect from site scripting method behaviour.
     */
    @Test
    public void protectFromSiteScriptingTest() {
        String actual = validator.protectFromSiteScripting(toProtect);

        assertEquals(wasProtected, actual);
    }
}