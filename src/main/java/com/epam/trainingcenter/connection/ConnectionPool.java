package com.epam.trainingcenter.connection;

import com.epam.trainingcenter.exception.ConnectionException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Thread safe connection pool.
 *
 * @author Oksana Morozova
 * @see Connection
 * @see ResourceBundle
 * @see ArrayBlockingQueue
 * @see Lock
 */
public class ConnectionPool {
    private static final Logger logger = Logger.getLogger(ConnectionPool.class);

    private static final ResourceBundle DATABASE = ResourceBundle.getBundle("database");
    private static final String URL = DATABASE.getString("db.url");
    private static final String USER = DATABASE.getString("db.user");
    private static final String PASSWORD = DATABASE.getString("db.password");

    private static final ConnectionPool POOL = new ConnectionPool();
    private ArrayBlockingQueue<Connection> connections;
    private final static int POOL_SIZE = 20;
    private Lock poolLocker;

    /**
     * Gets instance of connection pool class.
     *
     * @return the instance.
     */
    public static ConnectionPool getConnectionPool() {
        return POOL;
    }

    /**
     * Instantiates a new ConnectionPool.
     */
    private ConnectionPool() {
        connections = new ArrayBlockingQueue<>(POOL_SIZE);
        poolLocker = new ReentrantLock();

        for (int i = 0; i < POOL_SIZE; i++) {
            Connection connection;
            try {
                DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
                connections.offer(connection);
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    /**
     * Gets and removes connection from pool.
     *
     * @return connection from pool.
     */
    public Connection getConnection() throws ConnectionException {
        Connection connection;
        try {
            connection = connections.take();
        } catch (InterruptedException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ConnectionException(ex);
        }
        return connection;
    }

    /**
     * Adds chosen connection back to pool.
     *
     * @param connection to database, that was get from pool.
     */
    public void returnConnectionToPool(Connection connection) {
        if (connection != null) {
            try {
                poolLocker.lock();
                if (!connections.contains(connection)) {
                    connections.offer(connection);
                }
            } finally {
                poolLocker.unlock();
            }
        }
    }

    /**
     * Close all connections in pool.
     */
    public void closeAll() {
        for (Connection connection : connections) {
            try {
                connection.close();
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new ConnectionException(ex);
            }
        }
    }
}
