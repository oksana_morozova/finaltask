package com.epam.trainingcenter.entity;

/**
 * This class describes city of training.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class City extends AbstractEntity {
    private String city;

    /**
     * Gets city.
     *
     * @return the city.
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        City city = (City) obj;
        return city.city.equals(city);
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (city == null ? 0 : city.hashCode());
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "City{" +
                "city='" + city + '\'' +
                '}';
    }
}
