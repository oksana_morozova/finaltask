package com.epam.trainingcenter.entity;

/**
 * Role of user.
 *
 * @author Oksana Morozova
 * @see User
 */
public enum Role {
    STUDENT("STUDENT"), TEACHER("TEACHER"), ADMIN("ADMIN");
    private String value;

    /**
     * Instantiates a Role.
     *
     * @param value the role value.
     */
    Role(String value) {
        this.value = value;
    }

    /**
     * Gets role value.
     *
     * @return the role value.
     */
    public String getValue() {
        return value;
    }
}