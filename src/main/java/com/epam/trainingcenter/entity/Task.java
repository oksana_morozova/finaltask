package com.epam.trainingcenter.entity;

/**
 * This class describes task of the student.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class Task extends AbstractEntity {
    private String name;
    private String description;

    /**
     * Gets task name.
     *
     * @return the task name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets task name.
     *
     * @param name the task name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets task description.
     *
     * @return the task description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets task description.
     *
     * @param description the task description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        Task task = (Task) obj;
        return task.name.equals(name) &&
                task.description.equals(description);
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (name == null ? 0 : name.hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
