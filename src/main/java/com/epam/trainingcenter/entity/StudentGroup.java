package com.epam.trainingcenter.entity;

/**
 * This class describes many to many attitude between User and Group.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class StudentGroup extends AbstractEntity {
    private int studentId;
    private int groupId;

    /**
     * Gets student id.
     *
     * @return the student id.
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * Sets student id.
     *
     * @param studentId the student id.
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * Gets group id.
     *
     * @return the group id.
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * Sets group id.
     *
     * @param groupId the group id.
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        StudentGroup studentGroup = (StudentGroup) obj;
        return studentGroup.studentId == studentId &&
                studentGroup.groupId == groupId;
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + studentId;
        result = prime * result + groupId;
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "StudentGroup{" +
                "studentId=" + studentId +
                ", groupId=" + groupId +
                '}';
    }
}
