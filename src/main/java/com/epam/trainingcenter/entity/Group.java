package com.epam.trainingcenter.entity;

/**
 * This class describes group of training.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class Group extends AbstractEntity {
    private int trainingId;
    private int addressId;
    private int teacherId;

    /**
     * Gets training id.
     *
     * @return the training id.
     */
    public int getTrainingId() {
        return trainingId;
    }

    /**
     * Sets training id.
     *
     * @param trainingId the training id.
     */
    public void setTrainingId(int trainingId) {
        this.trainingId = trainingId;
    }

    /**
     * Gets address id.
     *
     * @return the address id.
     */
    public int getAddressId() {
        return addressId;
    }

    /**
     * Sets address id.
     *
     * @param addressId the address id.
     */
    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    /**
     * Gets teacher id.
     *
     * @return the teacher id.
     */
    public int getTeacherId() {
        return teacherId;
    }

    /**
     * Sets teacher id.
     *
     * @param teacherId the teacher id.
     */
    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        Group group = (Group) obj;
        return group.trainingId == trainingId &&
                group.addressId == addressId &&
                group.teacherId == teacherId;
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + trainingId;
        result = prime * result + addressId;
        result = prime * result + teacherId;
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "Group{" +
                "trainingId=" + trainingId +
                ", addressId=" + addressId +
                ", teacherId=" + teacherId +
                '}';
    }
}
