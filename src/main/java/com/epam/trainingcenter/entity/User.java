package com.epam.trainingcenter.entity;

/**
 * This class describes user.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class User extends AbstractEntity {
    private String password;
    private String name;
    private String surname;
    private String email;
    private Role role;

    /**
     * Gets user's password.
     *
     * @return the user's password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets user's password.
     *
     * @param password the user's password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets user's name.
     *
     * @return the user's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets user's name.
     *
     * @param name the user's name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets user's surname.
     *
     * @return the user's surname.
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets user's surname.
     *
     * @param surname the user's surname.
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Gets user's email.
     *
     * @return the user's email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets user's email.
     *
     * @param email the user's email.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets user's role.
     *
     * @return the user's role.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets user's role.
     *
     * @param role the user's role.
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        User user = (User) obj;
        return user.password.equals(password) &&
                user.name.equals(name) &&
                user.surname.equals(surname) &&
                user.email.equals(email) &&
                user.role == role;
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (password == null ? 0 : password.hashCode());
        result = prime * result + (name == null ? 0 : name.hashCode());
        result = prime * result + (surname == null ? 0 : surname.hashCode());
        result = prime * result + (email == null ? 0 : email.hashCode());
        result = prime * result + (role == null ? 0 : role.hashCode());
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "User{" +
                "password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", roleId=" + role +
                '}';
    }
}
