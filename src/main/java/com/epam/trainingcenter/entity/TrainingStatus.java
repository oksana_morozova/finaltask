package com.epam.trainingcenter.entity;

/**
 * Status of training.
 *
 * @author Oksana Morozova
 * @see Training
 */
public enum TrainingStatus {
    CLOSED_RECRUITMENT("CLOSED_RECRUITMENT"), FORTHCOMING_RECRUITMENT("FORTHCOMING_RECRUITMENT"),
    UNDER_RECRUITMENT("UNDER_RECRUITMENT");
    private String value;

    /**
     * Instantiates a TrainingStatus.
     *
     * @param value the trainingStatus value.
     */
    TrainingStatus(String value) {
        this.value = value;
    }

    /**
     * Gets trainingStatus value.
     *
     * @return the trainingStatus value.
     */
    public String getValue() {
        return value;
    }
}
