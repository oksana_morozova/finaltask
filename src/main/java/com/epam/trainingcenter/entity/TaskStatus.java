package com.epam.trainingcenter.entity;

/**
 * Status of task.
 *
 * @author Oksana Morozova
 * @see Task
 */
public enum TaskStatus {
    IN_PROCESS("IN_PROCESS"), DONE("DONE"), CHECKED("CHECKED");
    private String value;

    /**
     * Instantiates a TaskStatus.
     *
     * @param value the taskStatus value.
     */
    TaskStatus(String value) {
        this.value = value;
    }

    /**
     * Gets taskStatus value.
     *
     * @return the taskStatus value.
     */
    public String getValue() {
        return value;
    }
}
