package com.epam.trainingcenter.entity;

/**
 * This class describes address of training.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class Address extends AbstractEntity {
    private String address;

    /**
     * Gets address.
     *
     * @return the address.
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address the address.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        Address address = (Address) obj;
        return address.address.equals(address);
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (address == null ? 0 : address.hashCode());
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "Address{" +
                "address='" + address + '\'' +
                '}';
    }
}
