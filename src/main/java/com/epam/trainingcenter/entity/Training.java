package com.epam.trainingcenter.entity;

/**
 * This class describes training.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class Training extends AbstractEntity {
    private String name;
    private String description;
    private TrainingStatus trainingStatus;
    private String duration;
    private int numberOfStudents;
    private int cityId;
    private String startDate;
    private String contactEmail;
    private String picturePath;

    /**
     * Gets training name.
     *
     * @return the training name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets training name.
     *
     * @param name the training name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets training description.
     *
     * @return the training description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets training description.
     *
     * @param description the training description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets training status.
     *
     * @return the training status.
     */
    public TrainingStatus getTrainingStatus() {
        return trainingStatus;
    }

    /**
     * Sets training status.
     *
     * @param trainingStatus the training status.
     */
    public void setTrainingStatus(TrainingStatus trainingStatus) {
        this.trainingStatus = trainingStatus;
    }

    /**
     * Gets training duration.
     *
     * @return the training duration.
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Sets training duration.
     *
     * @param duration the training duration.
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * Gets number of students for training.
     *
     * @return the number of students for training.
     */
    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    /**
     * Sets number of students for training.
     *
     * @param numberOfStudents the number of students for training.
     */
    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    /**
     * Gets city id.
     *
     * @return the city id.
     */
    public int getCityId() {
        return cityId;
    }

    /**
     * Sets city id.
     *
     * @param cityId the city id.
     */
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    /**
     * Gets start date of training.
     *
     * @return the start date of training.
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets start date of training.
     *
     * @param startDate the start date of training.
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets training contact email.
     *
     * @return the training contact email.
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * Sets training contact email.
     *
     * @param contactEmail the training contact email.
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * Gets training picture path.
     *
     * @return the training picture path.
     */
    public String getPicturePath() {
        return picturePath;
    }

    /**
     * Sets training picture path.
     *
     * @param picturePath the training picture path.
     */
    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        Training training = (Training) obj;
        return training.name.equals(name) &&
                training.description.equals(description) &&
                training.trainingStatus == trainingStatus &&
                training.duration.equals(duration) &&
                training.numberOfStudents == numberOfStudents &&
                training.cityId == cityId &&
                training.startDate.equals(startDate) &&
                training.contactEmail.equals(contactEmail) &&
                training.picturePath.equals(picturePath);
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + (name == null ? 0 : name.hashCode());
        result = prime * result + (description == null ? 0 : description.hashCode());
        result = prime * result + (trainingStatus == null ? 0 : trainingStatus.hashCode());
        result = prime * result + (duration == null ? 0 : duration.hashCode());
        result = prime * result + numberOfStudents;
        result = prime * result + cityId;
        result = prime * result + (startDate == null ? 0 : startDate.hashCode());
        result = prime * result + (contactEmail == null ? 0 : contactEmail.hashCode());
        result = prime * result + (picturePath == null ? 0 : picturePath.hashCode());
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "Training{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", statusId=" + trainingStatus +
                ", duration='" + duration + '\'' +
                ", numberOfStudents=" + numberOfStudents +
                ", cityId=" + cityId +
                ", startDate='" + startDate + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", picturePath='" + picturePath + '\'' +
                '}';
    }
}
