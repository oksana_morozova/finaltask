package com.epam.trainingcenter.entity;

/**
 * The type Entity.
 *
 * @author Oksana Morozova
 */
public abstract class AbstractEntity {
    private int id;

    /**
     * Gets entity's id.
     *
     * @return the entity's id.
     */
    public int getId() {
        return id;
    }

    /**
     * Sets entity's id.
     *
     * @param id the entity's id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        AbstractEntity abstractEntity = (AbstractEntity) obj;
        return abstractEntity.id == id;
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "AbstractEntity{" +
                "id=" + id +
                '}';
    }
}
