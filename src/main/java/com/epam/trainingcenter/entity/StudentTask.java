package com.epam.trainingcenter.entity;

/**
 * This class describes many to many attitude between User and Task.
 *
 * @author Oksana Morozova
 * @see AbstractEntity
 */
public class StudentTask {
    private int studentId;
    private int taskId;
    private TaskStatus taskStatus;
    private int mark;

    /**
     * Gets student id.
     *
     * @return the student id.
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * Sets student id.
     *
     * @param studentId the student id.
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * Gets task id.
     *
     * @return the task id.
     */
    public int getTaskId() {
        return taskId;
    }

    /**
     * Sets task id.
     *
     * @param taskId the task id.
     */
    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    /**
     * Gets task status.
     *
     * @return the task status.
     */
    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    /**
     * Sets task status.
     *
     * @param taskStatus the task status.
     */
    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    /**
     * Gets mark.
     *
     * @return the mark.
     */
    public int getMark() {
        return mark;
    }

    /**
     * Sets mark.
     *
     * @param mark the mark.
     */
    public void setMark(int mark) {
        this.mark = mark;
    }

    /**
     * This method equals two objects.
     *
     * @param obj the object.
     * @return true if objects are equal and false otherwise.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        if (!super.equals(obj))
            return false;
        StudentTask studentTask = (StudentTask) obj;
        return studentTask.studentId == studentId &&
                studentTask.taskId == taskId &&
                studentTask.taskStatus == taskStatus &&
                studentTask.mark == mark;
    }

    /**
     * This method calculate object's hashcode.
     *
     * @return hashcode of object.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + studentId;
        result = prime * result + taskId;
        result = prime * result + (taskStatus == null ? 0 : taskStatus.hashCode());
        result = prime * result + mark;
        return result;
    }

    /**
     * This method builds string information about object.
     *
     * @return string information about object.
     */
    @Override
    public String toString() {
        return "StudentTask{" +
                "studentId=" + studentId +
                ", taskId=" + taskId +
                ", taskStatusId=" + taskStatus +
                ", mark=" + mark +
                '}';
    }
}
