package com.epam.trainingcenter.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.trainingcenter.command.Command.COMMAND_PARAMETER;
import static com.epam.trainingcenter.command.Command.IS_RECORD_INSERTED;

/**
 * Filter to follow database records.
 *
 * @author Oksana Morozova
 */
public class CommandDatabaseRecordFilter implements Filter {
    private static final Logger logger = Logger.getLogger(CommandDatabaseRecordFilter.class);

    private static final String REDIRECT_PAGE_PATH = "REDIRECT_PAGE";
    private static final String REGISTER_COMMAND = "register";
    private static final String CHANGE_TASK_STATUS_COMMAND = "change_task_status";
    private static final String SET_MARK_COMMAND = "set_mark";
    private static final String ADD_NEW_TASK_COMMAND = "add_new_task";
    private static final String ADD_NEW_TEACHER_COMMAND = "add_new_teacher";
    private static final String DELETE_TEACHER_COMMAND = "delete_teacher";
    private static final String ADD_NEW_TRAINING_COMMAND = "add_new_training";
    private static final String DELETE_TRAINING_COMMAND = "delete_training";


    private static final List<String> commands = new ArrayList<>();

    static {
        commands.add(REGISTER_COMMAND);
        commands.add(CHANGE_TASK_STATUS_COMMAND);
        commands.add(SET_MARK_COMMAND);
        commands.add(ADD_NEW_TASK_COMMAND);
        commands.add(ADD_NEW_TEACHER_COMMAND);
        commands.add(DELETE_TEACHER_COMMAND);
        commands.add(ADD_NEW_TRAINING_COMMAND);
        commands.add(DELETE_TRAINING_COMMAND);
    }

    private String redirectPage;

    /**
     * This method initialize filters object.
     *
     * @param filterConfig the filters config.
     */
    @Override
    public void init(FilterConfig filterConfig) {
        redirectPage = filterConfig.getInitParameter(REDIRECT_PAGE_PATH);
    }

    /**
     * The method does main logic of filters.
     *
     * @param servletRequest  the servlet request.
     * @param servletResponse the servlet response.
     * @param filterChain     the filters chain of responsibility.
     * @throws IOException      object if execution of method is failed.
     * @throws ServletException object if execution of method is failed.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String currentCommand = httpServletRequest.getParameter(COMMAND_PARAMETER);
        HttpSession session = httpServletRequest.getSession();

        if (!commands.contains(currentCommand)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            if (session.getAttribute(IS_RECORD_INSERTED) != null) {
                logger.info(String.format("Command - %s can't be invoked again.", currentCommand));

                session.removeAttribute(IS_RECORD_INSERTED);
                HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + redirectPage);
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        }
    }

    /**
     * This method cleans filter resources.
     */
    @Override
    public void destroy() {
    }
}