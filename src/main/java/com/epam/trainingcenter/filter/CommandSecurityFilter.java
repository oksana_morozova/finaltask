package com.epam.trainingcenter.filter;

import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.trainingcenter.command.Command.COMMAND_PARAMETER;
import static com.epam.trainingcenter.command.Command.USER_ATTRIBUTE;

/**
 * Filter to follow user's role command.
 *
 * @author Oksana Morozova
 */
public class CommandSecurityFilter implements Filter {
    private static final Logger logger = Logger.getLogger(CommandSecurityFilter.class);

    private static final String MAIN_PAGE_PARAMETER = "MAIN_PAGE";

    private static final String COMMON_COMMAND_PATTERN = "common_";
    private static final String TEACHER_COMMAND_PATTERN = "teacher_";
    private static final String STUDENT_COMMAND_PATTERN = "student_";
    private static final String ADMIN_COMMAND_PATTERN = "admin_";

    private String redirectPage;

    /**
     * This method initialize filters object.
     *
     * @param filterConfig the filters config.
     */
    @Override
    public void init(FilterConfig filterConfig) {
        redirectPage = filterConfig.getInitParameter(MAIN_PAGE_PARAMETER);
    }

    /**
     * The method does main logic of filters.
     *
     * @param servletRequest  the servlet request.
     * @param servletResponse the servlet response.
     * @param filterChain     the filters chain of responsibility.
     * @throws IOException      object if execution of method is failed.
     * @throws ServletException object if execution of method is failed.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        String currentCommand = httpServletRequest.getParameter(COMMAND_PARAMETER);

        HttpSession session = httpServletRequest.getSession();
        User user = (User) session.getAttribute(USER_ATTRIBUTE);

        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        if (currentCommand.startsWith(COMMON_COMMAND_PATTERN)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            if (user == null) {
                logger.warn(String.format("Unexpected action from guest, command=%s.", currentCommand));
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + redirectPage);
            } else {
                Role userRole = user.getRole();
                boolean isAccessTrue = checkRole(userRole, currentCommand);

                if (!isAccessTrue) {
                    logger.warn(String.format("Unexpected action from user id=%d, command=%s.", user.getId(),
                            currentCommand));

                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + redirectPage);
                } else {
                    filterChain.doFilter(servletRequest, servletResponse);
                }
            }
        }
    }

    /**
     * This method cleans filter resources.
     */
    @Override
    public void destroy() {

    }

    /**
     * This method check the user's role according to role pattern.
     *
     * @param userRole the role of the user.
     * @param command  the command.
     * @return true if user role suits to the command and false otherwise.
     */
    private boolean checkRole(Role userRole, String command) {
        switch (userRole) {
            case ADMIN: {
                return command.startsWith(ADMIN_COMMAND_PATTERN);
            }
            case TEACHER: {
                return command.startsWith(TEACHER_COMMAND_PATTERN);
            }
            case STUDENT: {
                return command.startsWith(STUDENT_COMMAND_PATTERN);
            }
            default: {
                return false;
            }
        }
    }
}
