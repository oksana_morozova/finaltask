package com.epam.trainingcenter.filter;

import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.User;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.epam.trainingcenter.command.Command.USER_ATTRIBUTE;

/**
 * Filter to follow user's role and pages.
 *
 * @author Oksana Morozova
 */
public class PageSecurityFilter implements Filter {
    private static final Logger logger = Logger.getLogger(PageSecurityFilter.class);

    private static final String MAIN_PAGE_PARAMETER = "MAIN_PAGE";

    private static final String ADMIN_PAGE_PATH_PATTERN = ".*/jsp/admin/.*.jsp";
    private static final String TEACHER_PAGE_PATH_PATTERN = ".*/jsp/teacher/.*.jsp";
    private static final String STUDENT_PAGE_PATH_PATTERN = ".*/jsp/student/.*.jsp";
    private static final String COMMON_PAGE_PATH_PATTERN = ".*/jsp/common/.*.jsp*";

    private String redirectPage;

    /**
     * This method initialize filters object.
     *
     * @param filterConfig the filters config.
     * @throws ServletException object if execution of method is failed.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        redirectPage = filterConfig.getInitParameter(MAIN_PAGE_PARAMETER);
    }

    /**
     * The method does main logic of filters.
     *
     * @param servletRequest  the servlet request.
     * @param servletResponse the servlet response.
     * @param filterChain     the filters chain of responsibility.
     * @throws IOException      object if execution of method is failed.
     * @throws ServletException object if execution of method is failed.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        String currentPage = httpServletRequest.getServletPath();
        boolean isCommonJsp = checkPath(currentPage, COMMON_PAGE_PATH_PATTERN);

        if (isCommonJsp) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            HttpSession session = httpServletRequest.getSession();
            User user = (User) session.getAttribute(USER_ATTRIBUTE);
            if (user == null) {
                logger.warn(String.format("Unexpected action from guest, page=%s.", currentPage));
                httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + redirectPage);
            } else {
                Role currentRole = user.getRole();
                boolean isUserRightRole = checkRole(currentRole, currentPage);

                if (!isUserRightRole) {
                    logger.warn(String.format("Unexpected action from user id=%d, page=%s.",
                            user.getId(), currentPage));
                    httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + redirectPage);
                } else {
                    filterChain.doFilter(servletRequest, servletResponse);
                }
            }
        }
    }

    /**
     * This method cleans filter resources.
     */
    @Override
    public void destroy() {
    }

    /**
     * This method check the user's role according to role pattern.
     *
     * @param role     the role of the user.
     * @param pagePath the path of the page.
     * @return true if user role suits to the path of the page and false otherwise.
     */
    private boolean checkRole(Role role, String pagePath) {
        switch (role) {
            case ADMIN: {
                return checkPath(pagePath, ADMIN_PAGE_PATH_PATTERN);
            }
            case TEACHER: {
                return checkPath(pagePath, TEACHER_PAGE_PATH_PATTERN);
            }
            case STUDENT: {
                return checkPath(pagePath, STUDENT_PAGE_PATH_PATTERN);
            }
            default: {
                return false;
            }
        }
    }

    /**
     * This method check the path according to page pattern.
     *
     * @param path        the path of the page.
     * @param pagePattern the pattern of the page.
     * @return true if path suits to the pattern and false otherwise.
     */
    private boolean checkPath(String path, String pagePattern) {
        Pattern pattern = Pattern.compile(pagePattern);
        Matcher matcher = pattern.matcher(path);

        return matcher.matches();
    }
}
