package com.epam.trainingcenter.controller;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.CommandFactory;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.connection.ConnectionPool;
import com.epam.trainingcenter.util.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.trainingcenter.command.Command.MESSAGE_ATTRIBUTE;
import static com.epam.trainingcenter.util.MessageManager.NONE_MESSAGE_KEY;

/**
 * MVC pattern controller class.
 *
 * @author Oksana Morozova
 * @see Page
 * @see Command
 * @see HttpServletResponse
 * @see HttpServletRequest
 */
public class Controller extends HttpServlet {
    private static final Logger logger = Logger.getLogger(Controller.class);

    /**
     * Get method.
     *
     * @param request  the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException object if execution of method is failed.
     * @throws IOException      object if execution of method is failed.
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Post method.
     *
     * @param request  the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException object if execution of method is failed.
     * @throws IOException      object if execution of method is failed.
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method process requests.
     *
     * @param request  the HTTP request.
     * @param response the HTTP response.
     * @throws ServletException object if execution of method is failed.
     * @throws IOException      object if execution of method is failed.
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Page page;
        CommandFactory factory = new CommandFactory();
        Command command = factory.defineCommand(request);
        page = command.execute(request);
        boolean isRedirect = page.isToRedirect();
        if (isRedirect) {
            redirect(page, request, response);
        } else {
            forward(page, request, response);
        }
    }

    /**
     * This method redirects requests.
     *
     * @param page the Page class instance.
     * @param request  the HTTP request.
     * @param response the HTTP response.
     * @throws IOException object if execution of method is failed.
     */
    private void redirect(Page page, HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String url = page.getUrl();
        response.sendRedirect(request.getContextPath() + url);
    }

    /**
     * This method forwards requests.
     *
     * @param page the Page class instance.
     * @param request  the HTTP request.
     * @param response the HTTP response.
     * @throws IOException object if execution of method is failed.
     * @throws ServletException object if execution of method is failed.
     */
    private void forward(Page page, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = page.getUrl();

        String messageKey = page.getMessageKey();
        if (!NONE_MESSAGE_KEY.equals(messageKey)) {
            String message = MessageManager.getProperty(messageKey);
            request.setAttribute(MESSAGE_ATTRIBUTE, message);
        } else
            request.setAttribute(MESSAGE_ATTRIBUTE, null);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    /**
     * Destruction method.
     */
    @Override
    public void destroy() {
        ConnectionPool.getConnectionPool().closeAll();
    }
}
