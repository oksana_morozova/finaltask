package com.epam.trainingcenter.exception;

/**
 * The type of unchecked exception that thrown from connection-level.
 *
 * @author Oksana Morozova
 * @see Exception
 */
public class ConnectionException extends RuntimeException {

    /**
     * Instantiates a new ConnectionException.
     */
    public ConnectionException() {
        super();
    }

    /**
     * Instantiates a new ConnectionException.
     *
     * @param message the message.
     */
    public ConnectionException(String message) {
        super(message);
    }

    /**
     * Instantiates a new ConnectionException.
     *
     * @param message the message.
     * @param cause   the cause.
     */
    public ConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new ConnectionException.
     *
     * @param cause the cause.
     */
    public ConnectionException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new ConnectionException.
     *
     * @param message            the message.
     * @param cause              the cause.
     * @param enableSuppression  the enable suppression.
     * @param writableStackTrace the writable stacktrace.
     */
    protected ConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}