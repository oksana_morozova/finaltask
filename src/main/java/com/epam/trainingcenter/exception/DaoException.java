package com.epam.trainingcenter.exception;

/**
 * The type of checked exception that thrown from Dao-level.
 *
 * @author Oksana Morozova
 * @see Exception
 */
public class DaoException extends Exception {

    /**
     * Instantiates a new DaoException.
     */
    public DaoException() {
        super();
    }

    /**
     * Instantiates a new DaoException.
     *
     * @param message the message.
     */
    public DaoException(String message) {
        super(message);
    }

    /**
     * Instantiates a new DaoException.
     *
     * @param message the message.
     * @param cause   the cause.
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new DaoException.
     *
     * @param cause the cause.
     */
    public DaoException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new DaoException.
     *
     * @param message            the message.
     * @param cause              the cause.
     * @param enableSuppression  the enable suppression.
     * @param writableStackTrace the writable stack trace.
     */
    protected DaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
