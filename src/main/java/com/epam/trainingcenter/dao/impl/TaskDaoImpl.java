package com.epam.trainingcenter.dao.impl;

import com.epam.trainingcenter.dao.AbstractDao;
import com.epam.trainingcenter.entity.Task;
import com.epam.trainingcenter.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that provide access to the database and deal with Task entity.
 *
 * @author Oksana Morozova
 * @see AbstractDao
 * @see Task
 */
public class TaskDaoImpl extends AbstractDao<Task> {
    private static final Logger logger = Logger.getLogger(TaskDaoImpl.class);

    /**
     * Common queries.
     */
    private static final String SELECT_ALL_QUERY = "SELECT * FROM task";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM task WHERE id=?";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM task WHERE id=?";
    private static final String INSERT_ENTITY_QUERY = "INSERT INTO task (name, description) VALUES(?,?)";
    private static final String UPDATE_ENTITY_QUERY = "UPDATE task SET name=?, description=? WHERE id=?";
    private static final String CHANGE_TASK_STATUS_QUERY = "UPDATE studentTask SET taskStatus =? " +
            "WHERE studentTask.studentId=? AND studentTask.taskId = " +
            "(SELECT task.id FROM .task WHERE task.name=?)";
    private static final String SET_TASK_MARK_QUERY = "UPDATE studentTask SET taskStatus=?, mark=? " +
            "WHERE studentTask.studentId=? AND studentTask.taskId=" +
            "(SELECT task.id FROM task WHERE task.name=?)";
    private static final String SELECT_TASK_BY_NAME_AND_DESCRIPTION_QUERY = "SELECT * FROM task WHERE name=? AND " +
            "description=?";
    private static final String ADD_NEW_TASK_QUERY = "INSERT INTO studentTask (studentId, taskId) " +
            "SELECT studentGroup.studentId, task.id FROM studentGroup " +
            "LEFT JOIN task ON task.name=? WHERE studentGroup.groupId=?";

    private static final String NAME_COLUMN_LABEL = "name";
    private static final String DESCRIPTION_COLUMN_LABEL = "description";

    /**
     * Instantiates a new AbstractDao.
     *
     * @param connection the connection to database.
     */
    public TaskDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * This method changes status in the task.
     *
     * @param taskName   the name of the task.
     * @param taskStatus the status of the task.
     * @param userId     the id of the user.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean changeTaskStatus(String taskName, String taskStatus, String userId) throws DaoException {
        return executeQuery(CHANGE_TASK_STATUS_QUERY, taskStatus, userId, taskName);
    }

    /**
     * This method sets student mark for the task.
     *
     * @param taskName   the name of the task.
     * @param mark       the student mark for the task.
     * @param userId     the id of the user.
     * @param taskStatus the status of the task.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean SetTaskSMark(String taskName, String mark, String userId, String taskStatus) throws DaoException {
        return executeQuery(SET_TASK_MARK_QUERY, taskStatus, mark, userId, taskName);
    }

    /**
     * This method checks task for being unique.
     *
     * @param taskName    the name of the task.
     * @param description the description of the task.
     * @return true if task with set parameters exists and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean checkTaskForUnique(String taskName, String description) throws DaoException {
        try (PreparedStatement preparedStatement =
                     prepareStatementForQuery(SELECT_TASK_BY_NAME_AND_DESCRIPTION_QUERY, taskName, description)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method inserts a new task ans set it to the group.
     *
     * @param taskName    the name of the task.
     * @param description the description of the task.
     * @param groupId     the id of the group where task is needed to be set.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean addNewTask(String taskName, String description, String groupId) throws DaoException {
        boolean isAdded = addOnlyTask(taskName, description);
        if (!isAdded)
            return false;

        return executeQuery(ADD_NEW_TASK_QUERY, taskName, groupId);
    }

    /**
     * This method inserts a new task to database.
     *
     * @param taskName    the name of the task.
     * @param description the description of the task.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    private boolean addOnlyTask(String taskName, String description) throws DaoException {
        return executeQuery(INSERT_ENTITY_QUERY, taskName, description);
    }

    /**
     * This method gets entity parameters.
     *
     * @param entity the entity.
     * @return List object with parameters.
     */
    @Override
    protected List<String> getEntityParameters(Task entity) {
        List<String> parameters = new ArrayList<>();

        String name = entity.getName();
        parameters.add(name);

        String description = entity.getDescription();
        parameters.add(description);

        return parameters;
    }

    /**
     * This method builds Task object from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the Task object.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    protected Task buildEntity(ResultSet resultSet) throws DaoException {
        try {
            Task task = new Task();

            int id = resultSet.getInt(ID_COLUMN_LABEL);
            task.setId(id);

            String name = resultSet.getString(NAME_COLUMN_LABEL);
            task.setName(name);

            String description = resultSet.getString(DESCRIPTION_COLUMN_LABEL);
            task.setDescription(description);

            return task;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method initialize queries for common operations.
     *
     * @return Map object with queries.
     */
    @Override
    protected Map<String, String> initializeCommonQueries() {
        Map<String, String> commonQueries = new HashMap<>();

        commonQueries.put(SELECT_ALL_QUERY_KEY, SELECT_ALL_QUERY);
        commonQueries.put(SELECT_BY_ID_QUERY_KEY, SELECT_BY_ID_QUERY);
        commonQueries.put(DELETE_BY_ID_QUERY_KEY, DELETE_BY_ID_QUERY);
        commonQueries.put(INSERT_ENTITY_QUERY_KEY, INSERT_ENTITY_QUERY);
        commonQueries.put(UPDATE_ENTITY_QUERY_KEY, UPDATE_ENTITY_QUERY);

        return commonQueries;
    }
}
