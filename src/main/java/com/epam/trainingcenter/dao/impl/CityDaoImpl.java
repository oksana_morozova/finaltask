package com.epam.trainingcenter.dao.impl;

import com.epam.trainingcenter.dao.AbstractDao;
import com.epam.trainingcenter.entity.City;
import com.epam.trainingcenter.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that provide access to the database and deal with City entity.
 *
 * @author Oksana Morozova
 * @see AbstractDao
 * @see City
 */
public class CityDaoImpl extends AbstractDao<City> {
    private static final Logger logger = Logger.getLogger(CityDaoImpl.class);

    /**
     * Common queries.
     */
    private static final String SELECT_ALL_QUERY = "SELECT * FROM city";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM city WHERE id=?";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM city WHERE id=?";
    private static final String INSERT_ENTITY_QUERY = "INSERT INTO city (city) VALUES(?)";
    private static final String UPDATE_ENTITY_QUERY = "UPDATE city SET city=? WHERE id=?";

    private static final String CITY_COLUMN_LABEL = "city";

    /**
     * Instantiates a new AbstractDao.
     *
     * @param connection the connection to database.
     */
    public CityDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * This method gets entity parameters.
     *
     * @param entity the entity.
     * @return List object with parameters.
     */
    @Override
    protected List<String> getEntityParameters(City entity) {
        List<String> parameters = new ArrayList<>();

        String city = entity.getCity();
        parameters.add(city);

        return parameters;
    }

    /**
     * This method builds City object from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the City object.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    protected City buildEntity(ResultSet resultSet) throws DaoException {
        try {
            City city = new City();

            int id = resultSet.getInt(ID_COLUMN_LABEL);
            city.setId(id);

            String cityName = resultSet.getString(CITY_COLUMN_LABEL);
            city.setCity(cityName);

            return city;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method initialize queries for common operations.
     *
     * @return Map object with queries.
     */
    @Override
    protected Map<String, String> initializeCommonQueries() {
        Map<String, String> commonQueries = new HashMap<>();

        commonQueries.put(SELECT_ALL_QUERY_KEY, SELECT_ALL_QUERY);
        commonQueries.put(SELECT_BY_ID_QUERY_KEY, SELECT_BY_ID_QUERY);
        commonQueries.put(DELETE_BY_ID_QUERY_KEY, DELETE_BY_ID_QUERY);
        commonQueries.put(INSERT_ENTITY_QUERY_KEY, INSERT_ENTITY_QUERY);
        commonQueries.put(UPDATE_ENTITY_QUERY_KEY, UPDATE_ENTITY_QUERY);

        return commonQueries;
    }
}
