package com.epam.trainingcenter.dao.impl;

import com.epam.trainingcenter.dao.AbstractDao;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

/**
 * Class that provide access to the database and deal with User entity.
 *
 * @author Oksana Morozova
 * @see AbstractDao
 * @see User
 */
public class UserDaoImpl extends AbstractDao<User> {
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    /**
     * Common queries.
     */
    private static final String SELECT_ALL_QUERY = "SELECT * FROM user";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM user WHERE id=?";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM user WHERE id=?";
    private static final String INSERT_ENTITY_QUERY = "INSERT INTO user (password, name, surname, email, role) VALUES(?,?,?,?,?)";
    private static final String UPDATE_ENTITY_QUERY = "UPDATE user SET password=?, name=?, surname=?, email=?, role=? WHERE id=?";
    private static final String SELECT_USER_BY_EMAIL_AND_PASSWORD_QUERY = "SELECT * FROM user WHERE email=? AND password=?";
    private static final String SELECT_STUDENT_GROUP_INFO_QUERY = "SELECT training.name AS trainingName, " +
            "address.address, user.name, user.surname FROM trainingcenter.group " +
            "LEFT JOIN training ON `group`.trainingId = training.id " +
            "LEFT JOIN address ON `group`.addressId = address.id " +
            "LEFT JOIN user ON `group`.teacherId = user.id " +
            "WHERE `group`.id = (SELECT studentGroup.groupId FROM user " +
            "JOIN studentGroup ON studentGroup.studentId = user.id WHERE user.id = ?)";
    private static final String SELECT_STUDENT_TASKS_INFO_QUERY = "SELECT task.name, task.description, " +
            "studentTask.taskStatus, studentTask.mark FROM task " +
            "LEFT JOIN studentTask ON task.id = studentTask.taskId " +
            "WHERE studentTask.studentId IN (SELECT user.id FROM user " +
            "JOIN studentTask ON studentTask.studentId = user.id WHERE user.id = ?)";
    private static final String SELECT_TEACHER_TASKS_INFO_QUERY = "SELECT user.id AS studentId, user.name, " +
            "user.surname, training.name AS trainingName, `group`.id, task.name AS taskName, studentTask.taskStatus, " +
            "studentTask.mark FROM `group` " +
            "LEFT JOIN training ON `group`.trainingId = training.id " +
            "LEFT JOIN studentGroup ON `group`.id = studentGroup.groupId " +
            "LEFT JOIN user ON studentGroup.studentId = user.id " +
            "LEFT JOIN studentTask ON user.id = studentTask.studentId " +
            "LEFT JOIN task ON studentTask.taskId = task.id WHERE `group`.teacherId=? ORDER BY user.name, task.id";
    private static final String SELECT_USERS_BY_EMAIL_QUERY = "SELECT * FROM user WHERE email=?";

    private static final String EMAIL_COLUMN_LABEL = "email";
    private static final String PASSWORD_COLUMN_LABEL = "password";
    private static final String ROLE_COLUMN_LABEL = "role";
    private static final String NAME_COLUMN_LABEL = "name";
    private static final String SURNAME_COLUMN_LABEL = "surname";
    private static final String TRAINING_NAME_COLUMN_LABEL = "trainingName";
    private static final String ADDRESS_COLUMN_LABEL = "address";
    private static final String DESCRIPTION_COLUMN_LABEL = "description";
    private static final String TASK_STATUS_COLUMN_LABEL = "taskStatus";
    private static final String MARK_COLUMN_LABEL = "mark";
    private static final String TASK_NAME_COLUMN_LABEL = "taskName";
    private static final String STUDENT_ID_COLUMN_LABEL = "studentId";

    /**
     * Instantiates a new AbstractDao.
     *
     * @param connection the connection to database.
     */
    public UserDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * This method finds user by email and password.
     *
     * @param login    the email of the user.
     * @param password the password of the user.
     * @return Optional User object.
     * @throws DaoException object if execution of query is failed.
     */
    public Optional<User> findUserByEmailAndPassword(String login, String password) throws DaoException {
        try (PreparedStatement preparedStatement =
                     prepareStatementForQuery(SELECT_USER_BY_EMAIL_AND_PASSWORD_QUERY, login, password)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            User user = null;
            if (resultSet.next()) {
                user = buildEntity(resultSet);
            }
            if (user != null)
                return Optional.of(user);
            else
                return Optional.empty();

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method finds information about student group.
     *
     * @param id the student id.
     * @return Optional Map object with student's group information.
     * @throws DaoException object if execution of query is failed.
     */
    public Optional<Map<String, String>> findStudentGroupInformation(int id) throws DaoException {
        try (PreparedStatement preparedStatement = prepareStatementForQuery(SELECT_STUDENT_GROUP_INFO_QUERY, id)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            Map<String, String> result = new HashMap<>();
            if (resultSet.next()) {
                result = buildStudentGroupInformation(resultSet);
            }
            if (!result.isEmpty())
                return Optional.of(result);
            else
                return Optional.empty();

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method finds information about student tasks.
     *
     * @param id the student id.
     * @return Optional List object with Map objects of student's tasks information.
     * @throws DaoException object if execution of query is failed.
     */
    public Optional<List<Map<String, String>>> findStudentTasksInformation(int id) throws DaoException {
        try (PreparedStatement preparedStatement = prepareStatementForQuery(SELECT_STUDENT_TASKS_INFO_QUERY, id)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            Map<String, String> result;
            List<Map<String, String>> tasksList = new ArrayList<>();
            while (resultSet.next()) {
                result = buildStudentTasksInformation(resultSet);
                tasksList.add(result);
            }
            if (!tasksList.isEmpty())
                return Optional.of(tasksList);
            else
                return Optional.empty();

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method finds information about teacher tasks.
     *
     * @param id the teacher id.
     * @return Optional List object with Map objects of teacher's tasks information.
     * @throws DaoException object if execution of query is failed.
     */
    public Optional<List<Map<String, String>>> findTeacherTasksInformation(int id) throws DaoException {
        try (PreparedStatement preparedStatement = prepareStatementForQuery(SELECT_TEACHER_TASKS_INFO_QUERY, id)) {
            ResultSet resultSet = preparedStatement.executeQuery();

            Map<String, String> result;
            List<Map<String, String>> tasksList = new ArrayList<>();
            while (resultSet.next()) {
                result = buildTeacherTasksInformation(resultSet);
                tasksList.add(result);
            }

            if (!tasksList.isEmpty())
                return Optional.of(tasksList);
            else
                return Optional.empty();

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method checks a login for being unique.
     *
     * @param email the login.
     * @return true if user with set login exists and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean checkLoginForUnique(String email) throws DaoException {
        try (PreparedStatement preparedStatement = prepareStatementForQuery(SELECT_USERS_BY_EMAIL_QUERY, email)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method builds Map object with student's group information from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the Map object with training information.
     * @throws DaoException object if execution of query is failed.
     */
    private Map<String, String> buildStudentGroupInformation(ResultSet resultSet) throws DaoException {
        try {
            Map<String, String> result = new HashMap();

            String trainingName = resultSet.getString(TRAINING_NAME_COLUMN_LABEL);
            result.put(TRAINING_NAME_COLUMN_LABEL, trainingName);

            String address = resultSet.getString(ADDRESS_COLUMN_LABEL);
            result.put(ADDRESS_COLUMN_LABEL, address);

            String name = resultSet.getString(NAME_COLUMN_LABEL);
            result.put(NAME_COLUMN_LABEL, name);

            String surname = resultSet.getString(SURNAME_COLUMN_LABEL);
            result.put(SURNAME_COLUMN_LABEL, surname);

            return result;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method builds Map object with student's tasks information from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the Map object with training information.
     * @throws DaoException object if execution of query is failed.
     */
    private Map<String, String> buildStudentTasksInformation(ResultSet resultSet) throws DaoException {
        try {
            Map<String, String> result = new HashMap();

            String name = resultSet.getString(NAME_COLUMN_LABEL);
            result.put(NAME_COLUMN_LABEL, name);

            String description = resultSet.getString(DESCRIPTION_COLUMN_LABEL);
            result.put(DESCRIPTION_COLUMN_LABEL, description);

            String taskStatus = resultSet.getString(TASK_STATUS_COLUMN_LABEL);
            result.put(TASK_STATUS_COLUMN_LABEL, taskStatus.toLowerCase());

            int mark = resultSet.getInt(MARK_COLUMN_LABEL);
            result.put(MARK_COLUMN_LABEL, String.valueOf(mark));

            return result;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method builds Map object with teacher's tasks information from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the Map object with training information.
     * @throws DaoException object if execution of query is failed.
     */
    private Map<String, String> buildTeacherTasksInformation(ResultSet resultSet) throws DaoException {
        try {
            Map<String, String> result = new HashMap();

            String name = resultSet.getString(NAME_COLUMN_LABEL);
            result.put(NAME_COLUMN_LABEL, name);

            String surname = resultSet.getString(SURNAME_COLUMN_LABEL);
            result.put(SURNAME_COLUMN_LABEL, surname);

            String trainingName = resultSet.getString(TRAINING_NAME_COLUMN_LABEL);
            result.put(TRAINING_NAME_COLUMN_LABEL, trainingName);

            int mark = resultSet.getInt(MARK_COLUMN_LABEL);
            result.put(MARK_COLUMN_LABEL, String.valueOf(mark));

            String taskStatus = resultSet.getString(TASK_STATUS_COLUMN_LABEL);
            result.put(TASK_STATUS_COLUMN_LABEL, taskStatus.toLowerCase());

            int groupNumber = resultSet.getInt(ID_COLUMN_LABEL);
            result.put(ID_COLUMN_LABEL, String.valueOf(groupNumber));

            String taskName = resultSet.getString(TASK_NAME_COLUMN_LABEL);
            result.put(TASK_NAME_COLUMN_LABEL, taskName);

            int studentId = resultSet.getInt(STUDENT_ID_COLUMN_LABEL);
            result.put(STUDENT_ID_COLUMN_LABEL, String.valueOf(studentId));
            return result;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method gets entity parameters.
     *
     * @param entity the entity.
     * @return List object with parameters.
     */
    @Override
    protected List<String> getEntityParameters(User entity) {
        List<String> parameters = new ArrayList<>();

        String password = entity.getPassword();
        parameters.add(password);

        String name = entity.getName();
        parameters.add(name);

        String surname = entity.getSurname();
        parameters.add(surname);

        String email = entity.getEmail();
        parameters.add(email);

        Role role = entity.getRole();
        String roleValue = String.valueOf(role);
        parameters.add(roleValue);

        return parameters;
    }

    /**
     * This method builds User object from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the User object.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    protected User buildEntity(ResultSet resultSet) throws DaoException {
        try {
            User user = new User();

            int id = resultSet.getInt(ID_COLUMN_LABEL);
            user.setId(id);

            String password = resultSet.getString(PASSWORD_COLUMN_LABEL);
            user.setPassword(password);

            String name = resultSet.getString(NAME_COLUMN_LABEL);
            user.setName(name);

            String surname = resultSet.getString(SURNAME_COLUMN_LABEL);
            user.setSurname(surname);

            String email = resultSet.getString(EMAIL_COLUMN_LABEL);
            user.setEmail(email);

            String roleValue = resultSet.getString(ROLE_COLUMN_LABEL);
            Role role = Role.valueOf(roleValue);
            user.setRole(role);

            return user;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method initialize queries for common operations.
     *
     * @return Map object with queries.
     */
    @Override
    protected Map<String, String> initializeCommonQueries() {
        Map<String, String> commonQueries = new HashMap<>();

        commonQueries.put(SELECT_ALL_QUERY_KEY, SELECT_ALL_QUERY);
        commonQueries.put(SELECT_BY_ID_QUERY_KEY, SELECT_BY_ID_QUERY);
        commonQueries.put(DELETE_BY_ID_QUERY_KEY, DELETE_BY_ID_QUERY);
        commonQueries.put(INSERT_ENTITY_QUERY_KEY, INSERT_ENTITY_QUERY);
        commonQueries.put(UPDATE_ENTITY_QUERY_KEY, UPDATE_ENTITY_QUERY);

        return commonQueries;
    }
}
