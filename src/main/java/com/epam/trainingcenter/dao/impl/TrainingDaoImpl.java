package com.epam.trainingcenter.dao.impl;

import com.epam.trainingcenter.dao.AbstractDao;
import com.epam.trainingcenter.entity.Training;
import com.epam.trainingcenter.entity.TrainingStatus;
import com.epam.trainingcenter.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Class that provide access to the database and deal with Training entity.
 *
 * @author Oksana Morozova
 * @see AbstractDao
 * @see Training
 */
public class TrainingDaoImpl extends AbstractDao<Training> {
    private static final Logger logger = Logger.getLogger(TrainingDaoImpl.class);

    /**
     * Common queries.
     */
    private static final String SELECT_ALL_QUERY = "SELECT * FROM training";
    private static final String SELECT_BY_ID_QUERY = "SELECT * FROM training WHERE id=?";
    private static final String DELETE_BY_ID_QUERY = "DELETE FROM training WHERE id=?";
    private static final String INSERT_ENTITY_QUERY = "INSERT INTO training (name, description, trainingStatus, duration," +
            " numberOfStudents, cityId, startDate, contactEmail, picturePath) VALUES(?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_ENTITY_QUERY = "UPDATE training SET name=?, description=?, " +
            "trainingStatus=?, duration=?, numberOfStudents=?, cityId=?, startDate=?, contactEmail=?, picturePath=? WHERE id=?";
    private static final String SELECT_TRAINING_INFO_QUERY = "SELECT * FROM training " +
            "LEFT JOIN city ON training.cityId = city.id WHERE training.name=?";
    private static final String SELECT_TRAINING_BY_NAME_AND_DESCRIPTION_QUERY = "SELECT * FROM training WHERE name=? AND " +
            "description=?";

    private static final String NAME_COLUMN_LABEL = "name";
    private static final String DESCRIPTION_COLUMN_LABEL = "description";
    private static final String TRAINING_STATUS_COLUMN_LABEL = "trainingStatus";
    private static final String DURATION_COLUMN_LABEL = "duration";
    private static final String NUMBER_OF_STUDENTS_COLUMN_LABEL = "numberOfStudents";
    private static final String CITY_ID_COLUMN_LABEL = "cityId";
    private static final String START_DATE_COLUMN_LABEL = "startDate";
    private static final String CONTACT_EMAIL_COLUMN_LABEL = "contactEmail";
    private static final String PICTURE_PATH_COLUMN_LABEL = "picturePath";
    private static final String CITY_COLUMN_LABEL = "city";

    /**
     * Instantiates a new AbstractDao.
     *
     * @param connection the connection to database.
     */
    public TrainingDaoImpl(Connection connection) {
        super(connection);
    }

    /**
     * This method finds all information about training.
     *
     * @param trainingName the name of the training.
     * @return Optional Map object with training information.
     * @throws DaoException object if execution of query is failed.
     */
    public Optional<Map<String, String>> findTrainingInfo(String trainingName) throws DaoException {
        try (PreparedStatement preparedStatement =
                     prepareStatementForQuery(SELECT_TRAINING_INFO_QUERY, trainingName)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            Map<String, String> trainingInfo = new HashMap<>();

            if (resultSet.next()) {
                trainingInfo = buildTrainingInformation(resultSet);
            }
            if (!trainingInfo.isEmpty())
                return Optional.of(trainingInfo);
            else
                return Optional.empty();

        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method checks a training for being unique.
     *
     * @param name        the name of the training.
     * @param description the description of the training.
     * @return true if training with set parameters exists and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean checkTrainingForUnique(String name, String description) throws DaoException {
        try (PreparedStatement preparedStatement =
                     prepareStatementForQuery(SELECT_TRAINING_BY_NAME_AND_DESCRIPTION_QUERY, name, description)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            return resultSet.next();
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method inserts a new task ans set it to the group.
     *
     * @param name             the name of the training.
     * @param description      the description of the training.
     * @param recruitment      the recruitment status of the training.
     * @param duration         the duration of the training.
     * @param numberOfStudents the number of students for the training.
     * @param city             the city where training is going to be occurred.
     * @param startDate        the start date when training is going to be occurred.
     * @param email            the email of person who responsible for this training.
     * @param paths            the path of the training picture, if such exists.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    public boolean addNewTraining(String name, String description, String recruitment, String duration,
                                  String numberOfStudents, String city, String startDate, String email, String... paths)
            throws DaoException {
        String path;
        if (paths.length == 0) {
            path = "default.png";
        } else {
            path = paths[0];
        }

        return executeQuery(INSERT_ENTITY_QUERY, name, description, recruitment, duration,
                numberOfStudents, city, startDate, email, path);
    }

    /**
     * This method builds Map object with training information from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the Map object with training information.
     * @throws DaoException object if execution of query is failed.
     */
    private Map<String, String> buildTrainingInformation(ResultSet resultSet) throws DaoException {
        try {
            Map<String, String> result = new HashMap();

            Training training = buildEntity(resultSet);
            result.put(NAME_COLUMN_LABEL, training.getName());
            result.put(DESCRIPTION_COLUMN_LABEL, training.getDescription());
            result.put(TRAINING_STATUS_COLUMN_LABEL, training.getTrainingStatus().toString().toLowerCase());
            result.put(DURATION_COLUMN_LABEL, training.getDuration());
            result.put(NUMBER_OF_STUDENTS_COLUMN_LABEL, String.valueOf(training.getNumberOfStudents()));
            result.put(START_DATE_COLUMN_LABEL, training.getStartDate());
            result.put(CONTACT_EMAIL_COLUMN_LABEL, training.getContactEmail());
            String city = resultSet.getString(CITY_COLUMN_LABEL);
            result.put(CITY_COLUMN_LABEL, city);

            return result;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method gets entity parameters.
     *
     * @param entity the entity.
     * @return List object with parameters.
     */
    @Override
    protected List<String> getEntityParameters(Training entity) {
        List<String> parameters = new ArrayList<>();

        String name = entity.getName();
        parameters.add(name);

        String description = entity.getDescription();
        parameters.add(description);

        TrainingStatus trainingStatus = entity.getTrainingStatus();
        String statusValue = String.valueOf(trainingStatus);
        parameters.add(statusValue);

        String duration = entity.getDuration();
        parameters.add(duration);

        int numberOfStudents = entity.getNumberOfStudents();
        String numberOfStudentsValue = String.valueOf(numberOfStudents);
        parameters.add(numberOfStudentsValue);

        int cityId = entity.getCityId();
        String cityIdValue = String.valueOf(cityId);
        parameters.add(cityIdValue);

        String startDate = entity.getStartDate();
        parameters.add(startDate);

        String contactEmail = entity.getContactEmail();
        parameters.add(contactEmail);

        String picturePath = entity.getPicturePath();
        parameters.add(picturePath);

        return parameters;
    }

    /**
     * This method builds Training object from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the Training object.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    protected Training buildEntity(ResultSet resultSet) throws DaoException {
        try {
            Training training = new Training();

            int id = resultSet.getInt(ID_COLUMN_LABEL);
            training.setId(id);

            String name = resultSet.getString(NAME_COLUMN_LABEL);
            training.setName(name);

            String description = resultSet.getString(DESCRIPTION_COLUMN_LABEL);
            training.setDescription(description);

            String trainingStatusValue = resultSet.getString(TRAINING_STATUS_COLUMN_LABEL);
            TrainingStatus status = TrainingStatus.valueOf(trainingStatusValue);
            training.setTrainingStatus(status);

            String duration = resultSet.getString(DURATION_COLUMN_LABEL);
            training.setDuration(duration);

            int numberOfStudents = resultSet.getInt(NUMBER_OF_STUDENTS_COLUMN_LABEL);
            training.setNumberOfStudents(numberOfStudents);

            int cityId = resultSet.getInt(CITY_ID_COLUMN_LABEL);
            training.setCityId(cityId);

            String startDate = resultSet.getString(START_DATE_COLUMN_LABEL);
            training.setStartDate(startDate);

            String contactEmail = resultSet.getString(CONTACT_EMAIL_COLUMN_LABEL);
            training.setContactEmail(contactEmail);

            String picturePath = resultSet.getString(PICTURE_PATH_COLUMN_LABEL);
            training.setPicturePath(picturePath);

            return training;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method initialize queries for common operations.
     *
     * @return Map object with queries.
     */
    @Override
    protected Map<String, String> initializeCommonQueries() {
        Map<String, String> commonQueries = new HashMap<>();

        commonQueries.put(SELECT_ALL_QUERY_KEY, SELECT_ALL_QUERY);
        commonQueries.put(SELECT_BY_ID_QUERY_KEY, SELECT_BY_ID_QUERY);
        commonQueries.put(DELETE_BY_ID_QUERY_KEY, DELETE_BY_ID_QUERY);
        commonQueries.put(INSERT_ENTITY_QUERY_KEY, INSERT_ENTITY_QUERY);
        commonQueries.put(UPDATE_ENTITY_QUERY_KEY, UPDATE_ENTITY_QUERY);

        return commonQueries;
    }
}
