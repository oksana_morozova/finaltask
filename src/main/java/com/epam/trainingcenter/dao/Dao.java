package com.epam.trainingcenter.dao;

import com.epam.trainingcenter.entity.AbstractEntity;
import com.epam.trainingcenter.exception.DaoException;

import java.util.List;

/**
 * Dao interface has all main methods to work with entity.
 *
 * @param <T> The entity.
 * @author Oksana Morozova
 */
public interface Dao<T extends AbstractEntity> {

    /**
     * This method finds all entities.
     *
     * @return List of found objects.
     * @throws DaoException object if execution of query is failed.
     */
    List<T> selectAll() throws DaoException;

    /**
     * This method finds entity from database by id.
     *
     * @param id the entity's id.
     * @return the entity.
     * @throws DaoException object if execution of query is failed.
     */
    T selectById(int id) throws DaoException;

    /**
     * This method deletes entity from database by id.
     *
     * @param id entity id.
     * @throws DaoException object if execution of query is failed.
     */
    boolean deleteById(int id) throws DaoException;

    /**
     * This method insert entity in database.
     *
     * @param entity the entity.
     * @return boolean true if entity created successfully, otherwise false.
     * @throws DaoException object if execution of query is failed.
     */
    boolean insert(T entity) throws DaoException;

    /**
     * This method update entity in database.
     *
     * @param entity the entity.
     * @throws DaoException object if execution of query is failed.
     */
    boolean update(T entity) throws DaoException;
}
