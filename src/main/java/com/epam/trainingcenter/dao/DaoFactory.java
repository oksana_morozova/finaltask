package com.epam.trainingcenter.dao;

import com.epam.trainingcenter.connection.ConnectionPool;
import com.epam.trainingcenter.dao.impl.CityDaoImpl;
import com.epam.trainingcenter.dao.impl.TaskDaoImpl;
import com.epam.trainingcenter.dao.impl.TrainingDaoImpl;
import com.epam.trainingcenter.dao.impl.UserDaoImpl;
import com.epam.trainingcenter.exception.ConnectionException;
import com.epam.trainingcenter.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;

/**
 * Class of dao level to work with connection from pool.
 *
 * @author Oksana Morozova
 * @see Connection
 * @see ConnectionPool
 */
public class DaoFactory implements AutoCloseable {
    private static final Logger logger = Logger.getLogger(DaoFactory.class);

    private ConnectionPool connectionPool;
    private Connection connection;

    /**
     * Instantiates a new DaoFactory.
     *
     * @throws DaoException object if process of initialization was failed.
     */
    public DaoFactory() throws DaoException {
        try {
            connectionPool = ConnectionPool.getConnectionPool();
            connection = connectionPool.getConnection();
        } catch (ConnectionException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * Gets UserDaoImpl object.
     *
     * @return the UserDaoImpl instance.
     */
    public UserDaoImpl getUserDao() {
        return new UserDaoImpl(connection);
    }

    /**
     * Gets TrainingDaoImpl object.
     *
     * @return the TrainingDaoImpl instance.
     */
    public TrainingDaoImpl getTrainingDao() {
        return new TrainingDaoImpl(connection);
    }

    /**
     * Gets TaskDaoImpl object.
     *
     * @return the TaskDaoImpl instance.
     */
    public TaskDaoImpl getTaskDao() {
        return new TaskDaoImpl(connection);
    }

    /**
     * Gets CityDaoImpl object.
     *
     * @return the CityDaoImpl instance.
     */
    public CityDaoImpl getCityDao() {
        return new CityDaoImpl(connection);
    }

    /**
     * Implementation of AutoCloseable interface to work with try().
     */
    @Override
    public void close() {
        connectionPool.returnConnectionToPool(connection);
    }
}
