package com.epam.trainingcenter.dao;

import com.epam.trainingcenter.entity.AbstractEntity;
import com.epam.trainingcenter.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Abstract root class of Dao level that provides access to the database and deal with application entities.
 *
 * @param <T> the entity type.
 * @author Oksana Morozova
 * @see Connection
 * @see AbstractEntity
 */
public abstract class AbstractDao<T extends AbstractEntity> implements Dao<T> {
    private static final Logger logger = Logger.getLogger(AbstractDao.class);

    public static final String ID_COLUMN_LABEL = "id";
    public static final int EMPTY_RESULT = 0;
    public static final String NULL_PARAMETER = "null";
    public static final String SELECT_ALL_QUERY_KEY = "SELECT_ALL";
    public static final String SELECT_BY_ID_QUERY_KEY = "SELECT_BY_ID";
    public static final String DELETE_BY_ID_QUERY_KEY = "DELETE_BY_ID";
    public static final String INSERT_ENTITY_QUERY_KEY = "INSERT_ENTITY";
    public static final String UPDATE_ENTITY_QUERY_KEY = "UPDATE_ENTITY";

    protected Connection connection;
    private final Map<String, String> commonQueries;

    /**
     * Instantiates a new AbstractDao.
     *
     * @param connection the connection to database.
     */
    public AbstractDao(Connection connection) {
        this.connection = connection;
        this.commonQueries = initializeCommonQueries();
    }

    /**
     * This method finds all entities.
     *
     * @return List of found objects.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    public List<T> selectAll() throws DaoException {
        String sqlQuery = commonQueries.get(SELECT_ALL_QUERY_KEY);
        try (Statement statement = connection.createStatement()) {
            List<T> entities = new ArrayList<T>();
            ResultSet resultSet = statement.executeQuery(sqlQuery);
            while (resultSet.next()) {
                T entity = buildEntity(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method finds entity from database by id.
     *
     * @param id the entity's id.
     * @return the entity.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    public T selectById(int id) throws DaoException {
        String sqlQuery = commonQueries.get(SELECT_BY_ID_QUERY_KEY);
        try (PreparedStatement preparedStatement = prepareStatementForQuery(sqlQuery, id)) {
            T entity = null;

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                entity = buildEntity(resultSet);
            }

            return entity;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method deletes entity from database by id.
     *
     * @param id entity id.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    public boolean deleteById(int id) throws DaoException {
        String sqlQuery = commonQueries.get(DELETE_BY_ID_QUERY_KEY);
        return executeQuery(sqlQuery, id);
    }

    /**
     * This method insert entity in database.
     *
     * @param entity the entity.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    public boolean insert(T entity) throws DaoException {
        String sqlQuery = commonQueries.get(INSERT_ENTITY_QUERY_KEY);
        List<String> parameters = getEntityParameters(entity);
        return executeQuery(sqlQuery, parameters);
    }

    /**
     * This method update entity in database.
     *
     * @param entity the entity.
     * @return true if operation was made successfully and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    @Override
    public boolean update(T entity) throws DaoException {
        String sqlQuery = commonQueries.get(UPDATE_ENTITY_QUERY_KEY);
        List<String> parameters = getEntityParameters(entity);

        int entityId = entity.getId();
        String entityIdValue = String.valueOf(entityId);
        parameters.add(entityIdValue);

        return executeQuery(sqlQuery, parameters);
    }

    /**
     * This method executes query.
     *
     * @param sqlQuery   the sql query.
     * @param parameters the parameters.
     * @return true if result is expected and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    protected boolean executeQuery(String sqlQuery, Object... parameters) throws DaoException {
        try (PreparedStatement preparedStatement = prepareStatementForQuery(sqlQuery, parameters)) {
            int queryResult = preparedStatement.executeUpdate();
            return queryResult != EMPTY_RESULT;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method executes query.
     *
     * @param sqlQuery   the sql query.
     * @param parameters the parameters.
     * @return true if result is expected and false otherwise.
     * @throws DaoException object if execution of query is failed.
     */
    private boolean executeQuery(String sqlQuery, List<String> parameters) throws DaoException {
        try (PreparedStatement preparedStatement = preparedStatementForQuery(sqlQuery, parameters)) {
            int queryResult = preparedStatement.executeUpdate();

            return queryResult != EMPTY_RESULT;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method initialize PreparedStatement object and sets it's parameters.
     *
     * @param sqlQuery   the sql query.
     * @param parameters the sql getEntityParameters.
     * @return PreparedStatement object.
     * @throws DaoException object if execution of query is failed.
     */
    protected PreparedStatement prepareStatementForQuery(String sqlQuery, Object... parameters) throws DaoException {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);

            if (parameters != null) {
                int parameterIndex = 1;
                for (Object parameter : parameters) {
                    if (parameter == null) {
                        preparedStatement.setNull(parameterIndex, Types.NULL);
                    } else {
                        preparedStatement.setObject(parameterIndex, parameter);
                    }
                    parameterIndex++;
                }
            }

            return preparedStatement;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method initialize PreparedStatement object and sets it's parameters.
     *
     * @param sqlQuery   the sql query.
     * @param parameters the sql getEntityParameters.
     * @return PreparedStatement object.
     * @throws DaoException object if execution of query is failed.
     */
    private PreparedStatement preparedStatementForQuery(String sqlQuery, List<String> parameters) throws DaoException {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);

            if (parameters != null) {
                int parameterIndex = 1;
                for (String parameter : parameters) {
                    if (NULL_PARAMETER.equals(parameter)) {
                        preparedStatement.setNull(parameterIndex, Types.NULL);
                    } else {
                        preparedStatement.setString(parameterIndex, parameter);
                    }
                    parameterIndex++;
                }
            }
            return preparedStatement;
        } catch (SQLException ex) {
            logger.error(ex.getMessage(), ex);
            throw new DaoException(ex.getMessage(), ex);
        }
    }

    /**
     * This method gets entity's parameters.
     *
     * @param entity the entity.
     * @return List object with parameters.
     */
    protected abstract List<String> getEntityParameters(T entity);

    /**
     * This method builds entity from ResultSet object.
     *
     * @param resultSet the result set of statement.
     * @return the entity.
     * @throws DaoException object if execution of query is failed.
     */
    protected abstract T buildEntity(ResultSet resultSet) throws DaoException;

    /**
     * This method initialize queries for common operations.
     *
     * @return Map object with queries.
     */
    protected abstract Map<String, String> initializeCommonQueries();
}

