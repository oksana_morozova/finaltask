package com.epam.trainingcenter.util;

/**
 * Util class for cross-site scripting validation.
 *
 * @author Oksana Morozova
 */
public class SiteScriptingValidator {
    private static final String OPENING_BRACKET = "&lt;";
    private static final String CLOSING_BRACKET = "&gt;";

    /**
     * Replace all scripting symbols from the string and protect from cross-site scripting.
     *
     * @param data the string needed to be protected.
     * @return the protected from cross-site scripting string.
     */
    public String protectFromSiteScripting(String data) {
        if (data == null || data.isEmpty())
            return data;

        String[] array = data.split("");
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals("<"))
                array[i] = OPENING_BRACKET;
            if (array[i].equals(">"))
                array[i] = CLOSING_BRACKET;

            result.append(array[i]);
        }

        return result.toString();
    }

}
