package com.epam.trainingcenter.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class to validate task data.
 *
 * @author Oksana Morozova
 * @see com.epam.trainingcenter.entity.Task
 */
public class TaskDataValidator {
    private static final String MARK_PATTERN = "^[0-9]{1,2}$";
    private static final String TASK_NAME_PATTERN = ".{1,45}";
    private static final String DESCRIPTION_PATTERN = ".{1,500}";

    /**
     * This method validates mark.
     *
     * @param mark the mark for the task.
     * @return true if data is valid and false otherwise.
     */
    public boolean checkMark(String mark) {
        if (mark == null || mark.isEmpty()) {
            return false;
        }

        boolean isMarkValid = matchPattern(mark, MARK_PATTERN);
        if (isMarkValid) {
            if (Integer.valueOf(mark) > 10)
                return false;
        }

        return isMarkValid;
    }

    /**
     * This method validates task data.
     *
     * @param taskName    the task name.
     * @param description the task description.
     * @return true if data is valid and false otherwise.
     */
    public boolean checkData(String taskName, String description) {
        if (taskName == null || taskName.isEmpty()) {
            return false;
        }
        if (description == null || description.isEmpty()) {
            return false;
        }

        boolean isTaskNameValid = matchPattern(taskName, TASK_NAME_PATTERN);
        boolean isDescriptionValid = matchPattern(description, DESCRIPTION_PATTERN);

        return isTaskNameValid && isDescriptionValid;
    }

    /**
     * This method check the data according to pattern.
     *
     * @param data           the data.
     * @param currentPattern the pattern.
     * @return true if data suits to the pattern and false otherwise.
     */
    private boolean matchPattern(String data, String currentPattern) {
        Pattern pattern = Pattern.compile(currentPattern);
        Matcher matcher = pattern.matcher(data);

        return matcher.matches();
    }
}
