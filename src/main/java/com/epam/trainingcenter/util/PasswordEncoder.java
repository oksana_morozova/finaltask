package com.epam.trainingcenter.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Util class for encoding user password.
 *
 * @author Oksana Morozova
 * @see com.epam.trainingcenter.entity.User
 * @see DigestUtils
 */
public class PasswordEncoder {

    /**
     * Encode password using shaHex algorithm.
     *
     * @param password the user's password.
     * @return the encoded user's password.
     */
    public static String encode(String password) {

        return DigestUtils.shaHex(password);
    }
}
