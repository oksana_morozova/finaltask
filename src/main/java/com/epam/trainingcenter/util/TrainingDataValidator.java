package com.epam.trainingcenter.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class to validate training data.
 *
 * @author Oksana Morozova
 * @see com.epam.trainingcenter.entity.Training
 */
public class TrainingDataValidator {
    private static final String EMAIL_PATTERN = "^.+@.+\\..+$";
    private static final String NAME_PATTERN = ".{1,45}";
    private static final String DESCRIPTION_PATTERN = ".{1,500}";
    private static final String DURATION_PATTERN = "(^\\d-?\\d*\\s[A-Za-z\u0410-\u042f\u0430-\u044f]+){1,45}";
    private static final String NUMBER_OF_STUDENTS_PATTERN = "\\d+";
    private static final String START_DATE_PATTERN = "(^\\d+-[A-Za-z\u0410-\u042f\u0430-\u044f]+){1,45}";

    /**
     * This method validates training data.
     *
     * @param name             the name of the training.
     * @param description      the description of the training.
     * @param duration         the duration of the training.
     * @param numberOfStudents the number of students for the training.
     * @param startDate        the start date when training is going to be occurred.
     * @param email            the email of person who responsible for this training.
     * @return true if data is valid and false otherwise.
     */
    public boolean checkTrainingData(String name, String description, String duration, String numberOfStudents,
                                     String startDate, String email) {
        if (name == null || name.isEmpty()) {
            return false;
        }
        if (description == null || description.isEmpty()) {
            return false;
        }
        if (duration == null || duration.isEmpty()) {
            return false;
        }
        if (numberOfStudents == null || numberOfStudents.isEmpty()) {
            return false;
        }
        if (startDate == null || startDate.isEmpty()) {
            return false;
        }
        if (email == null || email.isEmpty()) {
            return false;
        }

        boolean isNameValid = matchPattern(name, NAME_PATTERN);
        boolean isDescriptionValid = matchPattern(description, DESCRIPTION_PATTERN);
        boolean isDurationValid = matchPattern(duration, DURATION_PATTERN);
        boolean isNumberOfStudentsValid = matchPattern(numberOfStudents, NUMBER_OF_STUDENTS_PATTERN);
        boolean isStartDateValid = matchPattern(startDate, START_DATE_PATTERN);
        boolean isEmailValid = matchPattern(email, EMAIL_PATTERN);

        return isNameValid && isDescriptionValid && isDurationValid && isNumberOfStudentsValid && isStartDateValid &&
                isEmailValid;
    }

    /**
     * This method check the data according to pattern.
     *
     * @param data           the data.
     * @param currentPattern the pattern.
     * @return true if data suits to the pattern and false otherwise.
     */
    private boolean matchPattern(String data, String currentPattern) {
        Pattern pattern = Pattern.compile(currentPattern);
        Matcher matcher = pattern.matcher(data);

        return matcher.matches();
    }
}
