package com.epam.trainingcenter.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class to validate training data.
 *
 * @author Oksana Morozova
 * @see com.epam.trainingcenter.entity.User
 */
public class UserDataValidator {
    private static final String LOGIN_PATTERN = "^.+@.+\\..+$";
    private static final String PASSWORD_PATTERN = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]{6,45}";
    private static final String NAME_PATTERN = "^([A-ZА-Я])[A-Za-zА-яа-я]{5,45}";

    /**
     * This method validates training data.
     *
     * @param login    the email of the user.
     * @param password the password of the user.
     * @param name     the name of the user.
     * @param surname  the surname of the user.
     * @return true if data is valid and false otherwise.
     */
    public boolean checkData(String login, String password, String name, String surname) {
        if (login == null || login.isEmpty()) {
            return false;
        }
        if (password == null || password.isEmpty()) {
            return false;
        }
        if (name == null || name.isEmpty()) {
            return false;
        }
        if (surname == null || surname.isEmpty()) {
            return false;
        }

        boolean isLoginValid = matchPattern(login, LOGIN_PATTERN);
        boolean isPasswordValid = matchPattern(password, PASSWORD_PATTERN);
        boolean isFirstNameValid = matchPattern(name, NAME_PATTERN);
        boolean isLastNameValid = matchPattern(surname, NAME_PATTERN);

        return isLoginValid && isFirstNameValid && isPasswordValid && isLastNameValid;
    }

    /**
     * This method check the data according to pattern.
     *
     * @param data           the data.
     * @param currentPattern the pattern.
     * @return true if data suits to the pattern and false otherwise.
     */
    private boolean matchPattern(String data, String currentPattern) {
        Pattern pattern = Pattern.compile(currentPattern);
        Matcher matcher = pattern.matcher(data);

        return matcher.matches();
    }
}
