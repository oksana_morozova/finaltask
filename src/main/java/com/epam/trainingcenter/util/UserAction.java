package com.epam.trainingcenter.util;

import com.epam.trainingcenter.entity.City;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.Training;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.CityService;
import com.epam.trainingcenter.service.TrainingService;
import com.epam.trainingcenter.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.epam.trainingcenter.command.Command.*;

/**
 * Util class for getting user data according to the role.
 *
 * @author Oksana Morozova
 * @see User
 * @see Role
 * @see UserService
 * @see TrainingService
 * @see CityService
 * @see HttpServletRequest
 */
public class UserAction {

    /**
     * This method gets data for student.
     *
     * @param id      the student id.
     * @param role    the role.
     * @param request the HttpServletRequest request.
     * @throws ServiceException
     */
    public static void studentAction(int id, Role role, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        UserService userService = new UserService();
        Optional<Map<String, String>> optionalGroup = userService.getUserGroupInformation(id);
        Optional<List<Map<String, String>>> optionalTaskList =
                userService.getUserTasksInformation(id, role);
        if (optionalGroup.isPresent()) {
            session.setAttribute(USER_GROUP_INFO_ATTRIBUTE, optionalGroup.get());
        } else {
            session.setAttribute(USER_GROUP_INFO_ATTRIBUTE, null);
        }
        if (optionalTaskList.isPresent()) {
            session.setAttribute(USER_TASKS_INFO_ATTRIBUTE, optionalTaskList.get());
        }
    }

    /**
     * This method gets data for teacher.
     *
     * @param id      the teacher id.
     * @param role    the role.
     * @param request the HttpServletRequest request.
     * @throws ServiceException
     */
    public static void teacherAction(int id, Role role, HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        UserService userService = new UserService();
        Optional<List<Map<String, String>>> optionalTeacherTasks =
                userService.getUserTasksInformation(id, role);
        if (optionalTeacherTasks.isPresent()) {
            session.setAttribute(TEACHER_TASKS_INFO_ATTRIBUTE, optionalTeacherTasks.get());
        } else {
            session.setAttribute(TEACHER_TASKS_INFO_ATTRIBUTE, null);
        }
    }

    /**
     * This method gets data for teacher.
     *
     * @param request the HttpServletRequest request.
     * @throws ServiceException
     */
    public static void adminAction(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        UserService userService = new UserService();
        Optional<List<User>> optionalTeachers =
                userService.getTeachersInformation();
        if (optionalTeachers.isPresent()) {
            session.setAttribute(TEACHERS_ATTRIBUTE, optionalTeachers.get());
        } else {
            session.setAttribute(TEACHERS_ATTRIBUTE, null);
        }

        TrainingService trainingService = new TrainingService();
        Optional<List<Training>> optionalTrainings = trainingService.findAllTrainings();
        if (optionalTrainings.isPresent()) {
            session.setAttribute(TRAININGS_ATTRIBUTE, optionalTrainings.get());
        } else {
            session.setAttribute(TRAININGS_ATTRIBUTE, null);
        }

        CityService cityService = new CityService();
        Optional<List<City>> optionalCities = cityService.findAllCities();
        if (optionalTrainings.isPresent()) {
            session.setAttribute(CITIES_ATTRIBUTE, optionalCities.get());
        } else {
            session.setAttribute(CITIES_ATTRIBUTE, null);
        }
    }
}
