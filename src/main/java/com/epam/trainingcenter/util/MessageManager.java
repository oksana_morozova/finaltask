package com.epam.trainingcenter.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Util class for changing JSP page's language.
 *
 * @author Oksana Morozova
 * @see ResourceBundle
 * @see Locale
 */
public class MessageManager {
    private static final String RU_LANGUAGE = "ru";
    private static final String RU_COUNTRY = "RU";
    public static final Locale DEFAULT_LOCALE = new Locale(RU_LANGUAGE, RU_COUNTRY);

    public static final String NONE_MESSAGE_KEY = "NONE";
    public static final String COMMAND_ERROR_MESSAGE_KEY = "message.commandError";
    public static final String LOGIN_ERROR_MESSAGE_KEY = "message.loginError";
    public static final String LOGIN_NOT_AVAILABLE_MESSAGE_KEY = "message.loginNotAvailable";
    public static final String INVALID_INPUT_DATA_MESSAGE_KEY = "message.invalidInputData";
    public static final String REGISTRATION_FAILED_MESSAGE_KEY = "message.registrationFailed";
    public static final String LOAD_TRAINING_ERROR_MESSAGE_KEY = "message.loadTrainingError";
    public static final String LOAD_TRAININGS_LIST_ERROR_MESSAGE_KEY = "message.loadTrainingsListError";
    public static final String CHANGE_TASK_STATUS_ERROR_MESSAGE_KEY = "message.changeTaskStatusError";
    public static final String TASK_NOT_AVAILABLE_MESSAGE_KEY = "message.taskNotAvailable";
    public static final String ADDING_TASK_FAILED_MESSAGE_KEY = "message.addingTaskFailed";
    public static final String SETTING_MARK_FAILED_MESSAGE_KEY = "message.settingMarkFailed";
    public static final String DELETING_ERROR_MESSAGE_KEY = "message.deletingFailed";
    public static final String TRAINING_NOT_AVAILABLE_MESSAGE_KEY = "message.addingTrainingFailed";
    public static final String ADDING_TRAINING_FAILED_MESSAGE_KEY = "message.trainingFailed";

    private static final String RESOURCE_FILE_NAME = "messages";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle(RESOURCE_FILE_NAME, DEFAULT_LOCALE);

    /**
     * Instantiates a new Page.
     */
    private MessageManager() {
    }

    /**
     * Gets property from resource file.
     *
     * @param key the key of property.
     * @return the property.
     */
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }

    /**
     * Change language of jsp page.
     *
     * @param locale the locale.
     */
    public static void changeLocale(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(RESOURCE_FILE_NAME, locale);
    }
}

