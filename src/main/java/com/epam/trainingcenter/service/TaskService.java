package com.epam.trainingcenter.service;

import com.epam.trainingcenter.dao.DaoFactory;
import com.epam.trainingcenter.dao.impl.TaskDaoImpl;
import com.epam.trainingcenter.exception.DaoException;
import com.epam.trainingcenter.exception.ServiceException;
import org.apache.log4j.Logger;

/**
 * Service class for Task entity.
 *
 * @author Oksana Morozova
 * @see com.epam.trainingcenter.entity.Task
 * @see DaoFactory
 * @see TaskDaoImpl
 */
public class TaskService {
    private static final Logger logger = Logger.getLogger(TaskService.class);

    /**
     * This method changes task status.
     *
     * @param taskName   the name of the task.
     * @param taskStatus the new task status.
     * @param userId     the id of user whose task status is going to be changed.
     * @return true if operation was made successfully and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean changeTaskStatus(String taskName, String taskStatus, String userId) throws ServiceException {
        boolean isChanged;

        try (DaoFactory factory = new DaoFactory()) {
            TaskDaoImpl dao = factory.getTaskDao();
            isChanged = dao.changeTaskStatus(taskName, taskStatus, userId);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during changing task status", ex);
        }
        return isChanged;
    }

    /**
     * This method sets mark.
     *
     * @param taskName   the name of the task.
     * @param mark       the mark for the task.
     * @param userId     the id of user whose mark is going to be set.
     * @param taskStatus the new task status.
     * @return true if operation was made successfully and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean setTaskMark(String taskName, String mark, String userId, String taskStatus) throws ServiceException {
        boolean isChanged;

        try (DaoFactory factory = new DaoFactory()) {
            TaskDaoImpl dao = factory.getTaskDao();
            isChanged = dao.SetTaskSMark(taskName, mark, userId, taskStatus);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during setting task mark", ex);
        }
        return isChanged;
    }

    /**
     * This method checks a task for being unique.
     *
     * @param taskName    the name of the task.
     * @param description the description of the task.
     * @return true if task with set parameters exists and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean checkTaskForUnique(String taskName, String description) throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            TaskDaoImpl dao = factory.getTaskDao();
            return dao.checkTaskForUnique(taskName, description);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during checking new task for being unique", ex);
        }
    }

    /**
     * This method adds a new task.
     *
     * @param taskName    the name of the task.
     * @param description the description of the task.
     * @param groupId     the group whose task it's going to be.
     * @return true if operation was made successfully and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean addNewTask(String taskName, String description, String groupId) throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            TaskDaoImpl dao = factory.getTaskDao();
            return dao.addNewTask(taskName, description, groupId);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during adding new task", ex);
        }
    }

}
