package com.epam.trainingcenter.service;

import com.epam.trainingcenter.dao.DaoFactory;
import com.epam.trainingcenter.dao.impl.TrainingDaoImpl;
import com.epam.trainingcenter.entity.Training;
import com.epam.trainingcenter.exception.DaoException;
import com.epam.trainingcenter.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service class for Training entity.
 *
 * @author Oksana Morozova
 * @see Training
 * @see DaoFactory
 * @see TrainingDaoImpl
 */
public class TrainingService {
    private static final Logger logger = Logger.getLogger(TrainingService.class);

    /**
     * This method gets all trainings.
     *
     * @return Optional List of Training objects.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<List<Training>> findAllTrainings() throws ServiceException {
        Optional<List<Training>> trainingList;

        try (DaoFactory factory = new DaoFactory()) {
            TrainingDaoImpl dao = factory.getTrainingDao();
            trainingList = Optional.of(dao.selectAll());
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during finding all trainings", ex);
        }
        return trainingList == null ? Optional.empty() : trainingList;
    }

    /**
     * This method gets information about training.
     *
     * @param trainingName the name of the training.
     * @return Optional Map with training information.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<Map<String, String>> findTrainingInfo(String trainingName) throws ServiceException {
        Optional<Map<String, String>> trainingInfo;

        try (DaoFactory factory = new DaoFactory()) {
            TrainingDaoImpl dao = factory.getTrainingDao();
            trainingInfo = dao.findTrainingInfo(trainingName);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during finding training info", ex);
        }
        return trainingInfo == null ? Optional.empty() : trainingInfo;
    }

    /**
     * This method deletes a training by id.
     *
     * @param id the training's id.
     * @return true if operation was made successfully and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean deleteTrainingById(String id) throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            TrainingDaoImpl dao = factory.getTrainingDao();
            return dao.deleteById(Integer.valueOf(id));
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during deleting the training", ex);
        }
    }

    /**
     * This method checks a task for being unique.
     *
     * @param name        the name of the training.
     * @param description the description of the training.
     * @return true if training with set parameters exists and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean checkTrainingForUnique(String name, String description) throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            TrainingDaoImpl dao = factory.getTrainingDao();
            return dao.checkTrainingForUnique(name, description);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during checking new training for being unique", ex);
        }
    }

    /**
     * This method adds a new training.
     *
     * @param name             the name of the training.
     * @param description      the description of the training.
     * @param recruitment      the recruitment status of the training.
     * @param duration         the duration of the training.
     * @param numberOfStudents the number of students for the training.
     * @param city             the city where training is going to be occurred.
     * @param startDate        the start date when training is going to be occurred.
     * @param email            the email of person who responsible for this training.
     * @return true if operation was made successfully and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean addTraining(String name, String description, String recruitment, String duration,
                               String numberOfStudents, String city, String startDate, String email)
            throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            TrainingDaoImpl dao = factory.getTrainingDao();
            return dao.addNewTraining(name, description, recruitment, duration, numberOfStudents, city, startDate,
                    email);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during adding new training", ex);
        }
    }
}
