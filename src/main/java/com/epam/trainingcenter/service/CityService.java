package com.epam.trainingcenter.service;

import com.epam.trainingcenter.dao.DaoFactory;
import com.epam.trainingcenter.dao.impl.CityDaoImpl;
import com.epam.trainingcenter.entity.City;
import com.epam.trainingcenter.exception.DaoException;
import com.epam.trainingcenter.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Optional;

/**
 * Service class for City entity.
 *
 * @author Oksana Morozova
 * @see City
 * @see DaoFactory
 * @see CityDaoImpl
 */
public class CityService {
    private static final Logger logger = Logger.getLogger(CityService.class);

    /**
     * This method finds all cities in database.
     *
     * @return Optional List of City objects.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<List<City>> findAllCities() throws ServiceException {
        Optional<List<City>> cities;

        try (DaoFactory factory = new DaoFactory()) {
            CityDaoImpl dao = factory.getCityDao();
            cities = Optional.of(dao.selectAll());
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during finding all cities", ex);
        }
        return cities == null ? Optional.empty() : cities;
    }
}
