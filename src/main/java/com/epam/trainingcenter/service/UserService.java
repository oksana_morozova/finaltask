package com.epam.trainingcenter.service;

import com.epam.trainingcenter.dao.DaoFactory;
import com.epam.trainingcenter.dao.impl.UserDaoImpl;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.DaoException;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.util.PasswordEncoder;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for User entity.
 *
 * @author Oksana Morozova
 * @see User
 * @see DaoFactory
 * @see UserDaoImpl
 */
public class UserService {
    private static final Logger logger = Logger.getLogger(UserService.class);

    /**
     * This method logins a user.
     *
     * @param login    the email of the user.
     * @param password the password of the user.
     * @return Optional User object.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<User> login(String login, String password) throws ServiceException {
        Optional<User> user;
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();
            password = PasswordEncoder.encode(password);
            user = dao.findUserByEmailAndPassword(login, password);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Failed login", ex);
        }
        return user == null ? Optional.empty() : user;
    }

    /**
     * This method gets information about user's group.
     *
     * @param id the id of the user.
     * @return Optional Map with user's group information.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<Map<String, String>> getUserGroupInformation(int id) throws ServiceException {
        Optional<Map<String, String>> userGroupInformation;
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();
            userGroupInformation = dao.findStudentGroupInformation(id);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Failed during getting user group information", ex);
        }
        return userGroupInformation == null ? Optional.empty() : userGroupInformation;

    }

    /**
     * This method gets information about user's tasks.
     *
     * @param id   the id of the user.
     * @param role the role of the user.
     * @return Optional List object with Map objects of user's tasks information.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<List<Map<String, String>>> getUserTasksInformation(int id, Role role) throws ServiceException {
        Optional<List<Map<String, String>>> userTasksInformation = null;
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();
            switch (role) {
                case STUDENT:
                    userTasksInformation = dao.findStudentTasksInformation(id);
                    break;
                case TEACHER:
                    userTasksInformation = dao.findTeacherTasksInformation(id);
                    break;
            }
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Failed during getting user task information", ex);
        }
        return userTasksInformation == null ? Optional.empty() : userTasksInformation;
    }

    /**
     * This method checks a login for being unique.
     *
     * @param email the login of the user.
     * @return true if set login exists and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean checkUserLoginForUnique(String email) throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();
            return dao.checkLoginForUnique(email);
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during check user login for unique operation.", ex);
        }
    }

    /**
     * This method registers a user.
     *
     * @param login    the email of the user.
     * @param password the password of the user.
     * @param name     the name of the user.
     * @param surname  the surname of the user.
     * @param roles    the role of the user.
     * @return Optional User object.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<User> register(String login, String password, String name, String surname, Role... roles)
            throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();

            User user = new User();
            user.setEmail(login);
            user.setPassword(password);
            user.setName(name);
            user.setSurname(surname);
            Role role;
            if (roles.length == 0) {
                role = Role.STUDENT;
            } else {
                role = roles[0];
            }
            user.setRole(role);
            boolean isOperationSuccessful = dao.insert(user);
            return isOperationSuccessful ? Optional.of(user) : Optional.empty();
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Exception during register operation.", ex);
        }
    }

    /**
     * This method gets all teachers.
     *
     * @return Optional List of User objects.
     * @throws ServiceException object if execution of method is failed.
     */
    public Optional<List<User>> getTeachersInformation() throws ServiceException {
        Optional<List<User>> teachers = null;
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();
            List<User> users = dao.selectAll();
            teachers = Optional.of(users.stream().filter(user ->
                    user.getRole() == Role.TEACHER).collect(Collectors.toList()));
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Failed during getting teachers information", ex);
        }
        return teachers == null ? Optional.empty() : teachers;
    }

    /**
     * This method deletes a teacher by id.
     *
     * @param id the teacher's id.
     * @return true if operation was made successfully and false otherwise.
     * @throws ServiceException object if execution of method is failed.
     */
    public boolean deleteTeacherById(String id) throws ServiceException {
        try (DaoFactory factory = new DaoFactory()) {
            UserDaoImpl dao = factory.getUserDao();
            return dao.deleteById(Integer.valueOf(id));
        } catch (DaoException ex) {
            logger.error(ex.getMessage(), ex);
            throw new ServiceException("Failed during deleting teacher operation.", ex);
        }
    }
}
