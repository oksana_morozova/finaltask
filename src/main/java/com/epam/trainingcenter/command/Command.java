package com.epam.trainingcenter.command;

import javax.servlet.http.HttpServletRequest;

/**
 * The interface Command. Realization of pattern - Command.
 *
 * @author Oksana Morozova
 */
public interface Command {

    /**
     * Parameters.
     */
    String COMMAND_PARAMETER = "command";
    String PASSWORD_PARAMETER = "password";
    String TRAINING_NAME_PARAMETER = "trainingName";
    String NAME_PARAMETER = "name";
    String SURNAME_PARAMETER = "surname";
    String EMAIL_PARAMETER = "email";
    String TASK_STATUS_PARAMETER = "taskStatus";
    String TASK_NAME_PARAMETER = "taskName";
    String ID_PARAMETER = "id";
    String MARK_PARAMETER = "mark";
    String DESCRIPTION_PARAMETER = "description";
    String LOCALE_PARAMETER = "locale";
    String USER_ID_PARAMETER = "userId";
    String RECRUITMENT_PARAMETER = "recruitment";
    String DURATION_PARAMETER = "duration";
    String NUMBER_OF_STUDENTS_PARAMETER = "numberOfStudents";
    String CITY_PARAMETER = "city";
    String START_DATE_PARAMETER = "startDate";

    /**
     * Attributes.
     */
    String USER_ATTRIBUTE = "user";
    String LIST_ATTRIBUTE = "list";
    String USER_GROUP_INFO_ATTRIBUTE = "userInfo";
    String USER_TASKS_INFO_ATTRIBUTE = "userTasks";
    String TEACHER_TASKS_INFO_ATTRIBUTE = "teacherTasks";
    String TRAINING_INFO_ATTRIBUTE = "trainingInfo";
    String IS_RECORD_INSERTED = "recordInserted";
    String MESSAGE_ATTRIBUTE = "message";
    String TEACHERS_ATTRIBUTE = "teachers";
    String TRAININGS_ATTRIBUTE = "trainings";
    String CITIES_ATTRIBUTE = "cities";

    /**
     * Need to be implemented by commands classes.
     *
     * @param request HttpServletRequest object.
     * @return redirect page.
     * @see HttpServletRequest
     */
    Page execute(HttpServletRequest request);
}
