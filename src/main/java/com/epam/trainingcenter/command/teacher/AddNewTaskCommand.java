package com.epam.trainingcenter.command.teacher;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TaskService;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.SiteScriptingValidator;
import com.epam.trainingcenter.util.TaskDataValidator;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.trainingcenter.util.MessageManager.*;

/**
 * Command to add a new task.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TaskService
 * @see TaskDataValidator
 * @see SiteScriptingValidator
 * @see UserAction
 * @see com.epam.trainingcenter.entity.Task
 */
public class AddNewTaskCommand implements Command {
    private static final Logger logger = Logger.getLogger(AddNewTaskCommand.class);

    /**
     * Implementation of command that user uses to add a new task.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing AddNewTaskCommand.");
        try {
            String groupId = request.getParameter(ID_PARAMETER);
            String taskName = request.getParameter(TASK_NAME_PARAMETER);
            String description = request.getParameter(DESCRIPTION_PARAMETER);
            String userId = request.getParameter(USER_ID_PARAMETER);

            TaskDataValidator taskDataValidator = new TaskDataValidator();
            boolean isTaskDataValid = taskDataValidator.checkData(taskName, description);
            if (!isTaskDataValid) {
                logger.error("Data for new task isn't valid.");
                return new Page(Page.TEACHER_PAGE_PATH, false, INVALID_INPUT_DATA_MESSAGE_KEY);
            }

            SiteScriptingValidator siteScriptingValidator = new SiteScriptingValidator();
            taskName = siteScriptingValidator.protectFromSiteScripting(taskName);
            description = siteScriptingValidator.protectFromSiteScripting(description);

            TaskService taskService = new TaskService();
            boolean isTaskNotUnique = taskService.checkTaskForUnique(taskName, description);
            if (isTaskNotUnique) {
                logger.error("Task isn't unique.");
                return new Page(Page.TEACHER_PAGE_PATH, false, TASK_NOT_AVAILABLE_MESSAGE_KEY);
            }

            boolean isTaskAdded = taskService.addNewTask(taskName, description, groupId);
            if (!isTaskAdded) {
                logger.error("Error during adding new task.");
                return new Page(Page.TEACHER_PAGE_PATH, false, ADDING_TASK_FAILED_MESSAGE_KEY);
            }
            UserAction.teacherAction(Integer.valueOf(userId), Role.TEACHER, request);
            HttpSession session = request.getSession();
            session.setAttribute(IS_RECORD_INSERTED, true);
            return new Page(Page.TEACHER_PAGE_PATH, true);
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
