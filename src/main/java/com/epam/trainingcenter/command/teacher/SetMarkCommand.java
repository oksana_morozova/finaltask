package com.epam.trainingcenter.command.teacher;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.TaskStatus;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TaskService;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.TaskDataValidator;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.epam.trainingcenter.util.MessageManager.INVALID_INPUT_DATA_MESSAGE_KEY;
import static com.epam.trainingcenter.util.MessageManager.SETTING_MARK_FAILED_MESSAGE_KEY;

/**
 * Command to set a mark.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TaskService
 * @see TaskDataValidator
 * @see UserAction
 * @see com.epam.trainingcenter.entity.Task
 */
public class SetMarkCommand implements Command {
    private static final Logger logger = Logger.getLogger(SetMarkCommand.class);

    /**
     * Implementation of command that user uses to set a mark.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the SetMarkCommand.");
        try {
            String taskName = request.getParameter(TASK_NAME_PARAMETER);
            String mark = request.getParameter(MARK_PARAMETER);
            String studentId = request.getParameter(ID_PARAMETER);
            String taskStatus = TaskStatus.CHECKED.toString();
            String teacherId = request.getParameter(USER_ID_PARAMETER);

            TaskDataValidator taskDataValidator = new TaskDataValidator();
            boolean isTaskDataValid = taskDataValidator.checkMark(mark);
            if (!isTaskDataValid) {
                logger.error("Mark isn't valid.");
                return new Page(Page.TEACHER_PAGE_PATH, false, INVALID_INPUT_DATA_MESSAGE_KEY);
            }

            TaskService taskService = new TaskService();
            boolean isSet = taskService.setTaskMark(taskName, mark, studentId, taskStatus);

            if (isSet) {
                HttpSession session = request.getSession();
                UserAction.teacherAction(Integer.valueOf(teacherId), Role.TEACHER, request);
                session.setAttribute(IS_RECORD_INSERTED, true);
                return new Page(Page.TEACHER_PAGE_PATH, true);
            } else {
                logger.error("Error during setting new mark");
                return new Page(Page.TEACHER_PAGE_PATH, false, SETTING_MARK_FAILED_MESSAGE_KEY);
            }
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
