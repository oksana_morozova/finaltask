package com.epam.trainingcenter.command;

import static com.epam.trainingcenter.util.MessageManager.NONE_MESSAGE_KEY;

/**
 * Class describes page url.
 *
 * @author Oksana Morozova
 */
public class Page {

    /**
     * Common pages.
     */
    public static final String MAIN_PAGE_PATH = "/jsp/common/mainPage.jsp";
    public static final String ERROR_PAGE_PATH = "/jsp/common/error.jsp";
    public static final String TRAINING_PAGE_PATH = "/jsp/common/trainingPage.jsp";

    /**
     * Students' pages.
     */
    public static final String STUDENT_PAGE_PATH = "/jsp/student/studentPage.jsp";

    /**
     * Teachers' pages.
     */
    public static final String TEACHER_PAGE_PATH = "/jsp/teacher/teacherPage.jsp";

    /**
     * Admin pages.
     */
    public static final String ADMIN_PAGE_PATH = "/jsp/admin/adminPage.jsp";

    private String url;
    private boolean toRedirect;
    private String messageKey;

    /**
     * Instantiates a new Page.
     *
     * @param url        the page url.
     * @param toRedirect boolean value of variable toRedirect.
     */
    public Page(String url, boolean toRedirect) {
        this.url = url;
        this.toRedirect = toRedirect;
        this.messageKey = NONE_MESSAGE_KEY;
    }

    /**
     * Instantiates a new Page.
     *
     * @param url the page url.
     */
    public Page(String url) {
        this.url = url;
    }

    /**
     * Instantiates a new Page.
     */
    public Page() {
    }

    /**
     * Instantiates a new Page.
     *
     * @param url        the page url.
     * @param toRedirect boolean value of variable toRedirect.
     * @param messageKey the message key.
     */
    public Page(String url, boolean toRedirect, String messageKey) {
        this.url = url;
        this.toRedirect = toRedirect;
        this.messageKey = messageKey;
    }

    /**
     * Gets page url.
     *
     * @return the page url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets page url.
     *
     * @param url the page url.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets toRedirect value.
     *
     * @return boolean value of toRedirect variable.
     */
    public boolean isToRedirect() {
        return toRedirect;
    }

    /**
     * Sets toRedirect value.
     *
     * @param toRedirect the boolean value.
     */
    public void setToRedirect(boolean toRedirect) {
        this.toRedirect = toRedirect;
    }

    /**
     * Gets message key.
     *
     * @return the message key.
     */
    public String getMessageKey() {
        return messageKey;
    }

    /**
     * Sets the message key.
     *
     * @param messageKey the message key.
     */
    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }
}