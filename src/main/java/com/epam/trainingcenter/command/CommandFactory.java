package com.epam.trainingcenter.command;

import com.epam.trainingcenter.command.admin.AddNewTeacherCommand;
import com.epam.trainingcenter.command.admin.AddNewTrainingCommand;
import com.epam.trainingcenter.command.admin.DeleteTeacherCommand;
import com.epam.trainingcenter.command.admin.DeleteTrainingCommand;
import com.epam.trainingcenter.command.common.*;
import com.epam.trainingcenter.command.student.ChangeTaskStatusCommand;
import com.epam.trainingcenter.command.teacher.AddNewTaskCommand;
import com.epam.trainingcenter.command.teacher.SetMarkCommand;
import com.epam.trainingcenter.util.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

import static com.epam.trainingcenter.command.Command.COMMAND_PARAMETER;
import static com.epam.trainingcenter.command.Command.MESSAGE_ATTRIBUTE;
import static com.epam.trainingcenter.util.MessageManager.COMMAND_ERROR_MESSAGE_KEY;

/**
 * Factory class for creation commands.
 *
 * @author Oksana Morozova
 * @see Command
 */
public class CommandFactory {
    private static final Logger logger = Logger.getLogger(CommandFactory.class);

    /**
     * This method defines commands and returns its instance.
     *
     * @param request the HttpServletRequest request.
     * @return the defined commands.
     */
    public Command defineCommand(HttpServletRequest request) {
        logger.info("Defining command in CommandFactory.");
        String action = request.getParameter(COMMAND_PARAMETER);
        Command currentCommand = new EmptyCommand();

        if (action == null || action.isEmpty()) {
            logger.warn(String.format("Command - %s, is empty.", action));
            return currentCommand;
        }

        try {
            action = action.toUpperCase();
            switch (action) {
                case "COMMON_LOGIN":
                    return new LoginCommand();
                case "COMMON_SHOW_TRAININGS":
                    return new ShowTrainingsCommand();
                case "COMMON_LOG_OUT":
                    return new LogoutCommand();
                case "COMMON_SHOW_TRAINING_INFO":
                    return new ShowTrainingInfoCommand();
                case "COMMON_REGISTER":
                    return new RegisterCommand();
                case "STUDENT_CHANGE_TASK_STATUS":
                    return new ChangeTaskStatusCommand();
                case "TEACHER_SET_MARK":
                    return new SetMarkCommand();
                case "TEACHER_ADD_NEW_TASK":
                    return new AddNewTaskCommand();
                case "COMMON_CHANGE_LANGUAGE":
                    return new ChangeLanguageCommand();
                case "ADMIN_ADD_NEW_TEACHER":
                    return new AddNewTeacherCommand();
                case "ADMIN_DELETE_TEACHER":
                    return new DeleteTeacherCommand();
                case "ADMIN_ADD_NEW_TRAINING":
                    return new AddNewTrainingCommand();
                case "ADMIN_DELETE_TRAINING":
                    return new DeleteTrainingCommand();
                default:
                    throw new IllegalArgumentException("Unknown command in Command factory.");
            }
        } catch (IllegalArgumentException ex) {
            logger.error(String.format("Command - %s, cause exception.", action) + ex);
            String message = String.format("%s %s", action, MessageManager.getProperty(COMMAND_ERROR_MESSAGE_KEY));
            request.setAttribute(MESSAGE_ATTRIBUTE, message);
        }
        return currentCommand;
    }
}


