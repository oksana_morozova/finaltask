package com.epam.trainingcenter.command.student;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TaskService;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.epam.trainingcenter.util.MessageManager.CHANGE_TASK_STATUS_ERROR_MESSAGE_KEY;

/**
 * Command to change task status.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TaskService
 * @see UserAction
 * @see com.epam.trainingcenter.entity.Task
 */
public class ChangeTaskStatusCommand implements Command {
    private static final Logger logger = Logger.getLogger(ChangeTaskStatusCommand.class);

    /**
     * Implementation of command that user uses to change task status.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing ChangeTaskStatusCommand.");
        try {
            String taskName = request.getParameter(TASK_NAME_PARAMETER);
            String taskStatus = request.getParameter(TASK_STATUS_PARAMETER).toUpperCase();
            String userId = request.getParameter(ID_PARAMETER);
            TaskService taskService = new TaskService();
            boolean isChanged = taskService.changeTaskStatus(taskName, taskStatus, userId);
            if (isChanged) {
                HttpSession session = request.getSession();
                UserAction.studentAction(Integer.valueOf(userId), Role.STUDENT, request);
                session.setAttribute(IS_RECORD_INSERTED, true);
                return new Page(Page.STUDENT_PAGE_PATH, false);
            } else {
                logger.error("Error during changing task status");
                return new Page(Page.STUDENT_PAGE_PATH, false, CHANGE_TASK_STATUS_ERROR_MESSAGE_KEY);
            }
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
