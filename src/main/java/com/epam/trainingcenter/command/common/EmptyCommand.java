package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Empty command redirects to main page if command wasn't identify.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 */
public class EmptyCommand implements Command {
    private static final Logger logger = Logger.getLogger(EmptyCommand.class);

    /**
     * Implementation of command which redirects to main page.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the EmptyCommand.");
        return new Page(Page.MAIN_PAGE_PATH, false);
    }
}
