package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

import static com.epam.trainingcenter.util.MessageManager.LOGIN_ERROR_MESSAGE_KEY;

/**
 * Login command.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see UserService
 * @see UserAction
 * @see com.epam.trainingcenter.entity.User
 */
public class LoginCommand implements Command {
    private static final Logger logger = Logger.getLogger(LoginCommand.class);

    /**
     * Implementation of command that user uses to login.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the LoginCommand.");
        try {
            String login = request.getParameter(EMAIL_PARAMETER);
            String password = request.getParameter(PASSWORD_PARAMETER);
            UserService userService = new UserService();
            Optional<User> optionalUser = userService.login(login, password);

            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                choosePageInformation(user, request, userService);
                HttpSession session = request.getSession();
                session.setAttribute(USER_ATTRIBUTE, user);
                String path = choosePath(user);
                return new Page(path, true);
            } else {
                logger.info("Incorrect data for logging or such user doesn't exist.");
                return new Page(Page.MAIN_PAGE_PATH, false, LOGIN_ERROR_MESSAGE_KEY);
            }

        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }

    private String choosePath(User user) {
        switch (user.getRole()) {
            case STUDENT:
                return Page.STUDENT_PAGE_PATH;
            case TEACHER:
                return Page.TEACHER_PAGE_PATH;
            case ADMIN:
                return Page.ADMIN_PAGE_PATH;
        }
        return Page.MAIN_PAGE_PATH;
    }

    private void choosePageInformation(User user, HttpServletRequest request, UserService userService)
            throws ServiceException {
        switch (user.getRole()) {
            case STUDENT:
                UserAction.studentAction(user.getId(), user.getRole(), request);
                break;
            case TEACHER:
                UserAction.teacherAction(user.getId(), user.getRole(), request);
                break;
            case ADMIN:
                UserAction.adminAction(request);
                break;
        }
    }
}
