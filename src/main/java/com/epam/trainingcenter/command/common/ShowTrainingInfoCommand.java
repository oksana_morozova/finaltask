package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TrainingService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Optional;

import static com.epam.trainingcenter.util.MessageManager.LOAD_TRAINING_ERROR_MESSAGE_KEY;

/**
 * Command to show information about training.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TrainingService
 * @see com.epam.trainingcenter.entity.Training
 */
public class ShowTrainingInfoCommand implements Command {
    private static final Logger logger = Logger.getLogger(ShowTrainingInfoCommand.class);

    /**
     * Implementation of command that user uses to get information about training.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the ShowTrainingInfoCommand.");
        try {
            String trainingName = request.getParameter(TRAINING_NAME_PARAMETER);
            TrainingService trainingService = new TrainingService();
            Optional<Map<String, String>> optionalTrainingInfo = trainingService.findTrainingInfo(trainingName);

            if (optionalTrainingInfo.isPresent()) {
                Map<String, String> trainingInfo = optionalTrainingInfo.get();
                HttpSession session = request.getSession();
                session.setAttribute(TRAINING_INFO_ATTRIBUTE, trainingInfo);
                return new Page(Page.TRAINING_PAGE_PATH, true);
            } else {
                logger.error("Error during load training information.");
                return new Page(Page.MAIN_PAGE_PATH, false, LOAD_TRAINING_ERROR_MESSAGE_KEY);
            }
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
