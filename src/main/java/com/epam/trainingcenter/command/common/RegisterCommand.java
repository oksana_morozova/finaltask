package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.PasswordEncoder;
import com.epam.trainingcenter.util.UserDataValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

import static com.epam.trainingcenter.util.MessageManager.*;

/**
 * Command to register a new user.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see UserService
 * @see UserDataValidator
 * @see PasswordEncoder
 * @see LoginCommand
 * @see com.epam.trainingcenter.entity.User
 */
public class RegisterCommand implements Command {
    private static final Logger logger = Logger.getLogger(RegisterCommand.class);

    /**
     * Implementation of command that user uses to register.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the RegisterCommand;");
        try {
            String name = request.getParameter(NAME_PARAMETER);
            String surname = request.getParameter(SURNAME_PARAMETER);
            String email = request.getParameter(EMAIL_PARAMETER);
            String password = request.getParameter(PASSWORD_PARAMETER);

            UserDataValidator userDataValidator = new UserDataValidator();
            boolean isUserDataValid = userDataValidator.checkData(email, password, name, surname);
            if (!isUserDataValid) {
                logger.error("Registration data isn't valid.");
                return new Page(Page.MAIN_PAGE_PATH, false, INVALID_INPUT_DATA_MESSAGE_KEY);
            }

            UserService userService = new UserService();
            boolean isLoginNotUnique = userService.checkUserLoginForUnique(email);
            if (isLoginNotUnique) {
                logger.error("Login isn't unique.");
                return new Page(Page.MAIN_PAGE_PATH, false, LOGIN_NOT_AVAILABLE_MESSAGE_KEY);
            }

            password = PasswordEncoder.encode(password);
            Optional<User> optionalUser = userService.register(email, password, name, surname);
            if (!optionalUser.isPresent()) {
                logger.error("Error during registration a new user.");
                return new Page(Page.MAIN_PAGE_PATH, false, REGISTRATION_FAILED_MESSAGE_KEY);
            }

            HttpSession session = request.getSession();
            session.setAttribute(IS_RECORD_INSERTED, true);
            return new LoginCommand().execute(request);
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
