package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.Training;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TrainingService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

import static com.epam.trainingcenter.util.MessageManager.LOAD_TRAININGS_LIST_ERROR_MESSAGE_KEY;

/**
 * Command to show all trainings.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TrainingService
 * @see com.epam.trainingcenter.entity.Training
 */
public class ShowTrainingsCommand implements Command {
    private static final Logger logger = Logger.getLogger(ShowTrainingsCommand.class);

    /**
     * Implementation of command that user uses to get information about all trainings.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the ShowTrainingsCommand.");
        try {
            TrainingService trainingService = new TrainingService();
            Optional<List<Training>> optionalTrainingList = trainingService.findAllTrainings();

            if (optionalTrainingList.isPresent()) {
                List<Training> trainingList = optionalTrainingList.get();
                HttpSession session = request.getSession();
                session.setAttribute(LIST_ATTRIBUTE, trainingList);
                return new Page(Page.MAIN_PAGE_PATH, false);
            } else {
                logger.error("Error during loading trainings information");
                return new Page(Page.MAIN_PAGE_PATH, false, LOAD_TRAININGS_LIST_ERROR_MESSAGE_KEY);
            }
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
