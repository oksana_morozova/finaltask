package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Logout command.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 */
public class LogoutCommand implements Command {
    private static final Logger logger = Logger.getLogger(LogoutCommand.class);

    /**
     * Implementation of command that user uses to logout.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the LogoutCommand.");

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        return new Page(Page.MAIN_PAGE_PATH, true);
    }
}
