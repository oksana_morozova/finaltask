package com.epam.trainingcenter.command.common;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.util.MessageManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import java.util.Locale;

import static com.epam.trainingcenter.util.MessageManager.DEFAULT_LOCALE;

/**
 * Command to delete a training.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 */
public class ChangeLanguageCommand implements Command {
    private static final Logger logger = Logger.getLogger(ChangeLanguageCommand.class);

    private static final String RU_LANGUAGE = "ru";
    private static final String US_LANGUAGE = "en";
    private static final String BY_LANGUAGE = "by";

    private static final String RU_COUNTRY = "RU";
    private static final String US_COUNTRY = "US";
    private static final String BY_COUNTRY = "BY";

    /**
     * Implementation of command that user uses to change language.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    public Page execute(HttpServletRequest request) {
        logger.info("Executing the ChangeLanguageCommand.");

        String localeValue = request.getParameter(LOCALE_PARAMETER);
        Locale locale;
        switch (localeValue) {
            case RU_LANGUAGE: {
                locale = new Locale(RU_LANGUAGE, RU_COUNTRY);
                break;
            }
            case US_LANGUAGE: {
                locale = new Locale(US_LANGUAGE, US_COUNTRY);
                break;
            }
            case BY_LANGUAGE: {
                locale = new Locale(BY_LANGUAGE, BY_COUNTRY);
                break;
            }
            default: {
                locale = DEFAULT_LOCALE;
                break;
            }
        }

        HttpSession session = request.getSession();
        Config.set(session, Config.FMT_LOCALE, locale);
        MessageManager.changeLocale(locale);

        return new Page(Page.MAIN_PAGE_PATH, true);
    }
}
