package com.epam.trainingcenter.command.admin;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.trainingcenter.util.MessageManager.DELETING_ERROR_MESSAGE_KEY;

/**
 * Command to delete a teacher.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see UserService
 * @see UserAction
 * @see com.epam.trainingcenter.entity.User
 */
public class DeleteTeacherCommand implements Command {
    private static final Logger logger = Logger.getLogger(DeleteTeacherCommand.class);

    /**
     * Implementation of command that admin uses to delete a teacher.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing DeleteTeacherCommand.");
        try {
            String id = request.getParameter(ID_PARAMETER);

            UserService userService = new UserService();
            boolean isDeleted = userService.deleteTeacherById(id);
            if (!isDeleted) {
                logger.error("Error during deleting the teacher");
                return new Page(Page.ADMIN_PAGE_PATH, false, DELETING_ERROR_MESSAGE_KEY);
            }

            UserAction.adminAction(request);
            HttpSession session = request.getSession();
            session.setAttribute(IS_RECORD_INSERTED, true);
            return new Page(Page.ADMIN_PAGE_PATH, false);
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
