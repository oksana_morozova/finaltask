package com.epam.trainingcenter.command.admin;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TrainingService;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.trainingcenter.util.MessageManager.DELETING_ERROR_MESSAGE_KEY;

/**
 * Command to delete a training.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TrainingService
 * @see UserAction
 * @see com.epam.trainingcenter.entity.Training
 */
public class DeleteTrainingCommand implements Command {
    private static final Logger logger = Logger.getLogger(DeleteTrainingCommand.class);

    /**
     * Implementation of command that admin uses to delete a training.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing DeleteTrainingCommand.");
        try {
            String id = request.getParameter(ID_PARAMETER);

            TrainingService trainingService = new TrainingService();
            boolean isDeleted = trainingService.deleteTrainingById(id);
            if (!isDeleted) {
                logger.error("Error during deleting the training.");
                return new Page(Page.ADMIN_PAGE_PATH, false, DELETING_ERROR_MESSAGE_KEY);
            }

            UserAction.adminAction(request);
            HttpSession session = request.getSession();
            session.setAttribute(IS_RECORD_INSERTED, true);
            return new Page(Page.ADMIN_PAGE_PATH, false);
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
