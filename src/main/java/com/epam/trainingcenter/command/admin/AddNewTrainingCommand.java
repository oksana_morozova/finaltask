package com.epam.trainingcenter.command.admin;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.TrainingService;
import com.epam.trainingcenter.util.TrainingDataValidator;
import com.epam.trainingcenter.util.UserAction;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.epam.trainingcenter.util.MessageManager.*;

/**
 * Command to add a new training.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see TrainingService
 * @see TrainingDataValidator
 * @see UserAction
 * @see com.epam.trainingcenter.entity.Training
 */
public class AddNewTrainingCommand implements Command {
    private static final Logger logger = Logger.getLogger(AddNewTrainingCommand.class);

    /**
     * Implementation of command that admin uses to add a new training.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing AddNewTrainingCommand.");
        try {
            String name = request.getParameter(NAME_PARAMETER);
            String description = request.getParameter(DESCRIPTION_PARAMETER);
            String recruitment = request.getParameter(RECRUITMENT_PARAMETER);
            String duration = request.getParameter(DURATION_PARAMETER);
            String numberOfStudents = request.getParameter(NUMBER_OF_STUDENTS_PARAMETER);
            String city = request.getParameter(CITY_PARAMETER);
            String startDate = request.getParameter(START_DATE_PARAMETER);
            String email = request.getParameter(EMAIL_PARAMETER);

            TrainingDataValidator trainingDataValidator = new TrainingDataValidator();
            boolean isTrainingDataValid = trainingDataValidator.checkTrainingData(name, description, duration,
                    numberOfStudents, startDate, email);
            if (!isTrainingDataValid) {
                logger.error("Training data isn't valid.");
                return new Page(Page.ADMIN_PAGE_PATH, false, INVALID_INPUT_DATA_MESSAGE_KEY);
            }

            TrainingService trainingService = new TrainingService();
            boolean isTrainingNotUnique = trainingService.checkTrainingForUnique(name, description);
            if (isTrainingNotUnique) {
                logger.error("Training isn't unique.");
                return new Page(Page.ADMIN_PAGE_PATH, false, TRAINING_NOT_AVAILABLE_MESSAGE_KEY);
            }

            boolean isTrainingAdded = trainingService.addTraining(name, description, recruitment, duration,
                    numberOfStudents, city, startDate, email);
            if (!isTrainingAdded) {
                logger.error("Error during adding a new training.");
                return new Page(Page.ADMIN_PAGE_PATH, false, ADDING_TRAINING_FAILED_MESSAGE_KEY);
            }

            UserAction.adminAction(request);
            HttpSession session = request.getSession();
            session.setAttribute(IS_RECORD_INSERTED, true);
            return new Page(Page.ADMIN_PAGE_PATH, false);
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
