package com.epam.trainingcenter.command.admin;

import com.epam.trainingcenter.command.Command;
import com.epam.trainingcenter.command.Page;
import com.epam.trainingcenter.entity.Role;
import com.epam.trainingcenter.entity.User;
import com.epam.trainingcenter.exception.ServiceException;
import com.epam.trainingcenter.service.UserService;
import com.epam.trainingcenter.util.PasswordEncoder;
import com.epam.trainingcenter.util.UserAction;
import com.epam.trainingcenter.util.UserDataValidator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.Optional;

import static com.epam.trainingcenter.util.MessageManager.*;

/**
 * Command to add a new teacher.
 *
 * @author Oksana Morozova
 * @see Command
 * @see HttpServletRequest
 * @see UserService
 * @see UserDataValidator
 * @see UserAction
 * @see com.epam.trainingcenter.entity.User
 */
public class AddNewTeacherCommand implements Command {
    private static final Logger logger = Logger.getLogger(AddNewTeacherCommand.class);

    /**
     * Implementation of command that admin uses to register a new teacher.
     *
     * @param request HttpServletRequest object.
     * @return page.
     */
    @Override
    public Page execute(HttpServletRequest request) {
        logger.info("Executing AddNewTeacherCommand.");
        try {
            String name = request.getParameter(NAME_PARAMETER);
            String surname = request.getParameter(SURNAME_PARAMETER);
            String email = request.getParameter(EMAIL_PARAMETER);
            String password = request.getParameter(PASSWORD_PARAMETER);

            UserDataValidator userDataValidator = new UserDataValidator();
            boolean isUserDataValid = userDataValidator.checkData(email, password, name, surname);
            if (!isUserDataValid) {
                logger.error("Registration teacher data isn't valid.");
                return new Page(Page.ADMIN_PAGE_PATH, false, INVALID_INPUT_DATA_MESSAGE_KEY);
            }

            UserService userService = new UserService();
            boolean isLoginNotUnique = userService.checkUserLoginForUnique(email);
            if (isLoginNotUnique) {
                logger.error("Login isn't unique.");
                return new Page(Page.ADMIN_PAGE_PATH, false, LOGIN_NOT_AVAILABLE_MESSAGE_KEY);
            }

            password = PasswordEncoder.encode(password);
            Optional<User> optionalUser = userService.register(email, password, name, surname, Role.TEACHER);
            if (!optionalUser.isPresent()) {
                logger.error("Error during registration a new teacher.");
                return new Page(Page.ADMIN_PAGE_PATH, false, REGISTRATION_FAILED_MESSAGE_KEY);
            }
            UserAction.adminAction(request);
            HttpSession session = request.getSession();
            session.setAttribute(IS_RECORD_INSERTED, true);
            return new Page(Page.ADMIN_PAGE_PATH, false);
        } catch (ServiceException ex) {
            logger.error(ex.getMessage(), ex);
            return new Page(Page.ERROR_PAGE_PATH, true);
        }
    }
}
