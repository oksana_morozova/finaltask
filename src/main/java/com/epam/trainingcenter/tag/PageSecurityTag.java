package com.epam.trainingcenter.tag;


import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Class describes page security tag.
 *
 * @author Oksana Morozova
 */
public class PageSecurityTag extends TagSupport {

    /**
     * This method starts tag.
     *
     * @return int constant SKIP_BODY.
     */
    @Override
    public int doStartTag() {
        HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();
        response.setHeader("Pragma", "No-cache");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setDateHeader("Expires", -1);
        return SKIP_BODY;
    }
}
