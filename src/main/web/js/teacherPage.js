var modal = document.getElementById("modal");
var close = document.getElementById("close");
var button = document.getElementById("new-task");

var nameChecked = false;
var descriptionChecked = false;

var nameRegEx = ".{1,45}";
var descriptionRegEx = ".{1,500}";

var nameInput = document.getElementById("taskName");
var descriptionInput = document.getElementById("description");
var submit = document.getElementById("submit-r");

button.onclick = function () {
    modal.style.display = "block";
}

close.onclick = function () {
    modal.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

function submitChange() {
    if (nameChecked && descriptionChecked) {
        submit.disabled = false;
        submit.classList.add("active");
    } else {
        submit.disabled = true;
        submit.classList.remove("active");
    }
}

nameInput.addEventListener("input", function (event) {
    if (!nameInput.value.match(nameRegEx)) {
        nameInput.setCustomValidity("The task name can contain only 45 symbols");
        nameInput.style.border = "0.06vw solid #fc6d62";
        nameChecked = false;
    } else {
        nameInput.setCustomValidity("");
        nameInput.style.border = "0.1vw solid #f2ccc3";
        nameChecked = true;
    }
    submitChange()
});

descriptionInput.addEventListener("input", function (event) {
    if (!descriptionInput.value.match(descriptionRegEx)) {
        descriptionInput.setCustomValidity("The description can contain only 500 symbols");
        descriptionInput.style.border = "0.06vw solid #fc6d62";
        descriptionChecked = false;
    } else {
        descriptionInput.setCustomValidity("");
        descriptionInput.style.border = "0.1vw solid #f2ccc3";
        descriptionChecked = true;
    }
    submitChange()
});
