var passwordChecked = false;
var passwordRepeatChecked = false;
var nameChecked = false;
var surnameChecked = false;
var emailChecked = false;
var checkBoxChecked = false;

var passwordRegEx = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]{6,45}";
var nameRegEx = "^([A-Z\u0410-\u042f])[A-Za-z\u0410-\u042f\u0430-\u044f]{5,45}";
var emailRegEx = "^.+@.+\..+$";

var userName = document.getElementById("name");
var surname = document.getElementById("surname");
var password = document.getElementById("password");
var passwordRepeat = document.getElementById("passwordRepeat");
var email = document.getElementById("userEmail");
var checkBox = document.getElementById("check-box");
var submit = document.getElementById("submit-r");

function submitChange() {
    if (nameChecked && surnameChecked && passwordChecked && emailChecked && passwordRepeatChecked && checkBoxChecked) {
        submit.disabled = false;
        submit.classList.add("active");
    } else {
        submit.disabled = true;
        submit.classList.remove("active");
    }
}

userName.addEventListener("input", function (event) {
    if (!userName.value.match(nameRegEx)) {
        userName.setCustomValidity("The name can contain only letters and have 5 symbols or more.");
        userName.style.border = "0.06vw solid #fc6d62";
        nameChecked = false;
    } else {
        userName.setCustomValidity("");
        userName.style.border = "0.1vw solid #f2ccc3";
        nameChecked = true;
    }
    submitChange()
});

surname.addEventListener("input", function (event) {
    if (!surname.value.match(nameRegEx)) {
        surname.setCustomValidity("The surname can contain only letters and have 5 symbols or more.");
        surname.style.border = "0.06vw solid #fc6d62";
        surnameChecked = false;
    } else {
        surname.setCustomValidity("");
        surname.style.border = "0.1vw solid #f2ccc3";
        surnameChecked = true;
    }
    submitChange()
});

password.addEventListener("input", function (event) {
    if (!password.value.match(passwordRegEx)) {
        password.setCustomValidity("Password must contain at least one lowercase letter, one uppercase letter and" +
            "one digital and 6 or more symbols at all.");
        password.style.border = "0.06vw solid #fc6d62";
        passwordChecked = false;
    } else {
        password.setCustomValidity("");
        password.style.border = "0.1vw solid #f2ccc3";
        passwordChecked = true;
    }
    submitChange()
});

passwordRepeat.addEventListener("input", function (event) {
    var pass = document.getElementById("password");
    if (passwordRepeat.value != pass.value) {
        passwordRepeat.setCustomValidity("Passwords must be equal.");
        passwordRepeat.style.border = "0.06vw solid #fc6d62";
        passwordRepeatChecked = false;
    } else {
        passwordRepeat.setCustomValidity("");
        passwordRepeat.style.border = "0.1vw solid #f2ccc3";
        passwordRepeatChecked = true;
    }
    submitChange()
});

email.addEventListener("input", function (event) {
    if (!email.value.match(emailRegEx)) {
        email.setCustomValidity("Email must be correct: e.g. example@mail.com");
        email.style.border = "0.06vw solid #fc6d62";
        emailChecked = false;
    } else {
        email.setCustomValidity("");
        email.style.border = "0.1vw solid #f2ccc3";
        emailChecked = true;
    }
    submitChange()
});

checkBox.addEventListener("input", function (event) {
    if (!checkBox.checked) {
        checkBox.setCustomValidity("You didn't give consent to your data processing.");
        checkBoxChecked = false;
    } else {
        checkBox.setCustomValidity("");
        checkBoxChecked = true;
    }
    submitChange();
});

