var addTeacherModal = document.getElementById("teacher-modal");
var addTeacherClose = document.getElementById("teacher-close");
var addTeacherButton = document.getElementById("add-teacher");
var addTrainingModal = document.getElementById("training-modal");
var addTrainingClose = document.getElementById("training-close");
var addTrainingButton = document.getElementById("add-training");

var passwordChecked = false;
var nameChecked = false;
var surnameChecked = false;
var emailChecked = false;
var trNameChecked = false;
var trDescriptionChecked = false;
var trDurationChecked = false;
var trNumberOfStudentsChecked = false;
var trStartDateChecked = false;
var trContactEmailChecked = false;

var passwordRegEx = "(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[A-Za-z0-9]{6,45}";
var nameRegEx = "^([A-Z\u0410-\u042f])[A-Za-z\u0410-\u042f\u0430-\u044f]{5,45}";
var emailRegEx = "^.+@.+\..+$";
var trNameRegEx = ".{1,45}";
var trDescriptionRegEx = ".{1,500}";
var trDurationRegEx = "(^\\d-?\\d*\\s[A-Za-z\u0410-\u042f\u0430-\u044f]+){1,45}";
var trNumberOfStudentsRegEx = "\\d+";
var trStartDateRegEx = "(^\\d+-[A-Za-z\u0410-\u042f\u0430-\u044f]+){1,45}";

var teacherName = document.getElementById("name");
var teacherSurname = document.getElementById("surname");
var teacherPassword = document.getElementById("password");
var teacherEmail = document.getElementById("userEmail");
var teacherSubmit = document.getElementById("submit-add-teacher");
var trName = document.getElementById("trainingName");
var trDescription = document.getElementById("description");
var trDuration = document.getElementById("duration");
var trNumberOfStudents = document.getElementById("numberOfStudents");
var trStartDate = document.getElementById("startDate");
var trContactEmail = document.getElementById("contactEmail");
var trainingSubmit = document.getElementById("submit-add-training");

addTeacherButton.onclick = function () {
    addTeacherModal.style.display = "block";
}

addTeacherClose.onclick = function () {
    addTeacherModal.style.display = "none";
}

addTrainingButton.onclick = function () {
    addTrainingModal.style.display = "block";
}

addTrainingClose.onclick = function () {
    addTrainingModal.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == addTeacherModal) {
        addTeacherModal.style.display = "none";
    }
    if (event.target == addTrainingModal) {
        addTrainingModal.style.display = "none";
    }
}

function submitChange() {
    if (nameChecked && surnameChecked && passwordChecked && emailChecked) {
        teacherSubmit.disabled = false;
        teacherSubmit.classList.add("active");
    } else {
        teacherSubmit.disabled = true;
        teacherSubmit.classList.remove("active");
    }
}

teacherName.addEventListener("input", function (event) {
    if (!teacherName.value.match(nameRegEx)) {
        teacherName.setCustomValidity("The name can contain only letters and have 5 symbols or more.");
        teacherName.style.border = "0.06vw solid #fc6d62";
        nameChecked = false;
    } else {
        teacherName.setCustomValidity("");
        teacherName.style.border = "0.1vw solid #f2ccc3";
        nameChecked = true;
    }
    submitChange()
});

teacherSurname.addEventListener("input", function (event) {
    if (!teacherSurname.value.match(nameRegEx)) {
        teacherSurname.setCustomValidity("The surname can contain only letters and have 5 symbols or more.");
        teacherSurname.style.border = "0.06vw solid #fc6d62";
        surnameChecked = false;
    } else {
        teacherSurname.setCustomValidity("");
        teacherSurname.style.border = "0.1vw solid #f2ccc3";
        surnameChecked = true;
    }
    submitChange()
});

teacherPassword.addEventListener("input", function (event) {
    if (!teacherPassword.value.match(passwordRegEx)) {
        teacherPassword.setCustomValidity("Password must contain at least one lowercase letter, one uppercase letter and" +
            "one digital and 6 or more symbols at all.");
        teacherPassword.style.border = "0.06vw solid #fc6d62";
        passwordChecked = false;
    } else {
        teacherPassword.setCustomValidity("");
        teacherPassword.style.border = "0.1vw solid #f2ccc3";
        passwordChecked = true;
    }
    submitChange()
});

teacherEmail.addEventListener("input", function (event) {
    if (!teacherEmail.value.match(emailRegEx)) {
        teacherEmail.setCustomValidity("Email must be correct: e.g. example@mail.com");
        teacherEmail.style.border = "0.06vw solid #fc6d62";
        emailChecked = false;
    } else {
        teacherEmail.setCustomValidity("");
        teacherEmail.style.border = "0.1vw solid #f2ccc3";
        emailChecked = true;
    }
    submitChange()
});

function submitChangeTr() {
    if (trNameChecked && trDescriptionChecked && trDurationChecked && trNumberOfStudentsChecked && trStartDateChecked
        && trContactEmailChecked) {
        trainingSubmit.disabled = false;
        trainingSubmit.classList.add("active");
    } else {
        trainingSubmit.disabled = true;
        trainingSubmit.classList.remove("active");
    }
}

trName.addEventListener("input", function (event) {
    if (!trName.value.match(trNameRegEx)) {
        trName.setCustomValidity("The training name can contain only 45 symbols");
        trName.style.border = "0.06vw solid #fc6d62";
        trNameChecked = false;
    } else {
        trName.setCustomValidity("");
        trName.style.border = "0.1vw solid #f2ccc3";
        trNameChecked = true;
    }
    submitChangeTr()
});

trDescription.addEventListener("input", function (event) {
    if (!trDescription.value.match(trDescriptionRegEx)) {
        trDescription.setCustomValidity("The description can contain only 500 symbols");
        trDescription.style.border = "0.06vw solid #fc6d62";
        trDescriptionChecked = false;
    } else {
        trDescription.setCustomValidity("");
        trDescription.style.border = "0.1vw solid #f2ccc3";
        trDescriptionChecked = true;
    }
    submitChangeTr()
});

trDuration.addEventListener("input", function (event) {
    if (!trDuration.value.match(trDurationRegEx)) {
        trDuration.setCustomValidity("Input duration as in example: 3-4 weeks");
        trDuration.style.border = "0.06vw solid #fc6d62";
        trDurationChecked = false;
    } else {
        trDuration.setCustomValidity("");
        trDuration.style.border = "0.1vw solid #f2ccc3";
        trDurationChecked = true;
    }
    submitChangeTr()
});

trNumberOfStudents.addEventListener("input", function (event) {
    if (!trNumberOfStudents.value.match(trNumberOfStudentsRegEx)) {
        trNumberOfStudents.setCustomValidity("Number of students must be integer and positive value");
        trNumberOfStudents.style.border = "0.06vw solid #fc6d62";
        trNumberOfStudentsChecked = false;
    } else {
        trNumberOfStudents.setCustomValidity("");
        trNumberOfStudents.style.border = "0.1vw solid #f2ccc3";
        trNumberOfStudentsChecked = true;
    }
    submitChangeTr()
});

trStartDate.addEventListener("input", function (event) {
    if (!trStartDate.value.match(trStartDateRegEx)) {
        trStartDate.setCustomValidity("Input start date as in example: 2019-October");
        trStartDate.style.border = "0.06vw solid #fc6d62";
        trStartDateChecked = false;
    } else {
        trStartDate.setCustomValidity("");
        trStartDate.style.border = "0.1vw solid #f2ccc3";
        trStartDateChecked = true;
    }
    submitChangeTr()
});

trContactEmail.addEventListener("input", function (event) {
    if (!trContactEmail.value.match(emailRegEx)) {
        trContactEmail.setCustomValidity("Email must be correct: e.g. example@mail.com");
        trContactEmail.style.border = "0.06vw solid #fc6d62";
        trContactEmailChecked = false;
    } else {
        trContactEmail.setCustomValidity("");
        trContactEmail.style.border = "0.1vw solid #f2ccc3";
        trContactEmailChecked = true;
    }
    submitChangeTr()
});

