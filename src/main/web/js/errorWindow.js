var erModal = document.getElementById("errorModal");
var erClose = document.getElementById("errorClose");

erClose.onclick = function () {
    erModal.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == erModal) {
        erModal.style.display = "none";
    }
}