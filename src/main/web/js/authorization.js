var logModal = document.getElementById("logModal");
var regModal = document.getElementById("regModal");
var logClose = document.getElementById("logClose");
var regClose = document.getElementById("regClose");
var button = document.getElementsByClassName("sign-in")[0];

button.onclick = function () {
    logModal.style.display = "block";
}

regClose.onclick = function () {
    regModal.style.display = "none";
}

logClose.onclick = function () {
    logModal.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == logModal) {
        logModal.style.display = "none";
    }
    if (event.target == regModal) {
        regModal.style.display = "none";
    }
}

function signWhat(object) {
    var id = object.getAttribute("id");
    if (id == "toRegistration") {
        logModal.style.display = "none";
        regModal.style.display = "block";
    }
    if (id == "toLogin") {
        regModal.style.display = "none";
        logModal.style.display = "block";
    }
}