<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="footer.text" var="footer"/>
</fmt:bundle>

<link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">

<div class="container">
    <div class="footer-copyrights"></div>
    <div class="copyright-text">${pageScope.footer}</div>
    <div class="footer-contact facebook"></div>
    <div class="footer-contact twitter"></div>
    <div class="footer-contact chat"></div>
</div>
