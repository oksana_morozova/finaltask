<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="error" var="error"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<div id="root">
    <div id="errorModal" class="modal er">
        <div class="modal-content">
            <div class="modal-header">
                <div class="auth-text logging">${pageScope.error}</div>
                <span class="close errorCl" id="errorClose">&times;</span>
            </div>
            <div class="errorInfo"> ${requestScope.message}</div>
        </div>
    </div>
</div>
</body>
</html>