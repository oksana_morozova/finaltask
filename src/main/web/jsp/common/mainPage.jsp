<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="JAVA" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="mainPage.title.name" var="title"/>
    <fmt:message key="mainPage.greeting.text" var="greeting"/>
    <fmt:message key="mainPage.viewTrainings" var="trainings"/>
    <fmt:message key="mainPage.showButton" var="button"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${pageScope.title}</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png" type="image/png">
    <link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
</head>
<body>
<c:if test="${requestScope.message ne null}">
    <jsp:include page="errorWindow.jsp"/>
</c:if>
<div id="root">
    <header id="header">
        <jsp:include page="header.jsp"/>
    </header>
    <div class="home-page">
        <div class="greeting-banner">
            <div class="greeting-text">${pageScope.greeting}</div>
        </div>
        <section class="training_list">
            <form method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="common_show_trainings">
                <input type="hidden" name="page" value="main_page">
                <button class="show-trainings" type="submit">${pageScope.trainings}</button>
            </form>
            <div class="training-list-container">
                <c:forEach var="training" items="${sessionScope.list}">
                    <div class="training-item">
                        <div class="training-item-inner">
                            <div class="training-item-icon">
                                <img src="${pageContext.request.contextPath}/img/${training.picturePath}" alt="Icon">
                            </div>
                            <div class="training-item-title">${training.name}</div>
                            <div class="training-item-duration">
                                <div class="left-align-text">${training.startDate}</div>
                                <div class="right-align-text">${training.duration}</div>
                            </div>
                            <div class="training-item-button">
                                <form method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                                    <input type="hidden" name="command" value="common_show_training_info">
                                    <input type="hidden" name="trainingName" value="${training.name}">
                                    <button class="show-btn" type="submit">${pageScope.button}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </section>
    </div>
    <footer id="footer">
        <tag:footer/>
    </footer>
</div>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/authorization.js"></script>
<script src="${pageContext.request.contextPath}/js/registration.js"></script>
<script src="${pageContext.request.contextPath}/js/errorWindow.js"></script>
</html>