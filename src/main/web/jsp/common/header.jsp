<%@ page contentType="text/html;charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="header.en" var="en"/>
    <fmt:message key="header.ru" var="ru"/>
    <fmt:message key="header.by" var="by"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="logo-container">
        <a href="${pageContext.request.contextPath}/jsp/mainPage.jsp" class="our-logo"></a>
    </div>
    <div class="language-selector">
        <details>
            <summary></summary>
            <br>
            <a href="${pageContext.request.contextPath}/trainingcenter?command=common_change_language&locale=en">
                ${pageScope.en}
            </a>
            <a href="${pageContext.request.contextPath}/trainingcenter?command=common_change_language&locale=ru">
                ${pageScope.ru}
            </a>
            <a href="${pageContext.request.contextPath}/trainingcenter?command=common_change_language&locale=by">
                ${pageScope.by}
            </a>
        </details>
    </div>
    <div class="sign-in-container">
        <button class="sign-in"></button>
    </div>
</div>
<div id="sign-in">
    <jsp:include page="authorization.jsp"/>
    <jsp:include page="registration.jsp"/>
</div>
</body>
</html>