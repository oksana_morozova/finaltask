<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="authorizationPage.signIn" var="signIn"/>
    <fmt:message key="authorizationPage.placeholder.password" var="placeholderPass"/>
    <fmt:message key="authorizationPage.register.question" var="authQuestion"/>
    <fmt:message key="authorizationPage.register.text" var="authText"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${pageContext.request.contextPath}/css/authorization.css" rel="stylesheet">
</head>
<body>
<div id="root">
    <div id="logModal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <div class="auth-text logging">${pageScope.signIn}</div>
                <span class="close" id="logClose">&times;</span>
            </div>
            <form class="modal-container log" method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="common_login">
                <div class="group">
                    <input required class="field" type="email" id="logUserEmail" placeholder="mail@example.com"
                           name="email">
                </div>
                <div class="group">
                    <input required class="field" type="password" id="logPassword"
                           placeholder=" ${pageScope.placeholderPass}" name="password">
                </div>
                <div class="group">
                    <button class="submit-button" type="submit">${pageScope.signIn}</button>
                </div>
                <div class="group">
                    <div class="authorisation-text" id="toRegistration">${pageScope.authQuestion}
                        <span class="authorisation" onclick="return signWhat(this.parentNode)">
                            ${pageScope.authText}
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>