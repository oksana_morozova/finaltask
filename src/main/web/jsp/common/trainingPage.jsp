<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="trainingPage.duration" var="duration"/>
    <fmt:message key="trainingPage.numberOfStudents" var="students"/>
    <fmt:message key="trainingPage.startDate" var="startDate"/>
    <fmt:message key="trainingPage.city" var="city"/>
    <fmt:message key="trainingPage.trainingStatus" var="status"/>
    <fmt:message key="trainingPage.contactEmail" var="email"/>
    <fmt:message key="trainingPage.applyInfo" var="applyInfo"/>
    <fmt:message key="trainingPage.noApplyInfo" var="noApplyInfo"/>
    <fmt:message key="newTraining.underRecruitment" var="underRecruitment"/>
    <fmt:message key="newTraining.forthcomingRecruitment" var="forthcomingRecruitment"/>
    <fmt:message key="newTraining.closedRecruitment" var="closedRecruitment"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${sessionScope.trainingInfo.get("name")}</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png" type="image/png">
    <link href="${pageContext.request.contextPath}/css/mainPage.css" rel="stylesheet">
</head>
<body>
<div id="root">
    <header id="header">
        <jsp:include page="header.jsp"/>
    </header>
    <div class="home-page">
        <div class="training-container">
            <div class="name">
                ${sessionScope.trainingInfo.get("name")}
            </div>
            <div class="small-text">
                ${pageScope.duration} ${sessionScope.trainingInfo.duration} |
                ${pageScope.students} ${sessionScope.trainingInfo.numberOfStudents} |
                ${pageScope.startDate} ${sessionScope.trainingInfo.startDate}
            </div>
            <div class="info">
                ${pageScope.city} ${sessionScope.trainingInfo.city}
            </div>
            <div class="info">
                ${pageScope.status}
                <c:choose>
                    <c:when test="${sessionScope.trainingInfo.trainingStatus == \"closed_recruitment\"}">
                        ${pageScope.closedRecruitment}
                    </c:when>
                    <c:when test="${sessionScope.trainingInfo.trainingStatus == \"under_recruitment\"}">
                        ${pageScope.underRecruitment}
                    </c:when>
                    <c:when test="${sessionScope.trainingInfo.trainingStatus == \"forthcoming_recruitment\"}">
                        ${pageScope.forthcomingRecruitment}
                    </c:when>
                </c:choose>
            </div>
            <div class="small-text">${sessionScope.trainingInfo.description}</div>
            <div class="info">${pageScope.email} ${sessionScope.trainingInfo.contactEmail}</div>
            <div class="info apply">
                <br><br>
                <c:choose>
                    <c:when test="${sessionScope.trainingInfo.trainingStatus != \"closed_recruitment\"}">
                        ${pageScope.applyInfo}
                    </c:when>
                    <c:otherwise>
                        ${pageScope.noApplyInfo}
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
    <footer id="footer">
        <tag:footer/>
    </footer>
</div>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/authorization.js"></script>
</html>