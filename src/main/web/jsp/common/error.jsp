<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>
<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="error.title" var="title"/>
    <fmt:message key="error.request" var="requestMessage"/>
    <fmt:message key="error.servlet" var="servletMessage"/>
    <fmt:message key="error.status" var="statusMessage"/>
    <fmt:message key="error.exception" var="exceptionMessage"/>
</fmt:bundle>

<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/mainPage.css">
    <title>${pageScope.title}</title>
</head>
<body>
<div class="error_wrapper">
    <p>${pageScope.requestMessage} ${pageContext.errorData.requestURI} is failed</p>
    <p>${pageScope.servletMessage} ${pageContext.errorData.servletName}</p>
    <p>${pageScope.statusMessage} ${pageContext.errorData.statusCode}</p>
    <p>${pageScope.exceptionMessage} ${pageContext.errorData.throwable}</p>
</div>
</body>
</html>
