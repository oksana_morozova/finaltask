<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="registrationPage.registration" var="registration"/>
    <fmt:message key="registrationPage.placeholder.name" var="placeholderName"/>
    <fmt:message key="registrationPage.title.name" var="titleName"/>
    <fmt:message key="registrationPage.placeholder.surname" var="placeholderSurname"/>
    <fmt:message key="registrationPage.title.surname" var="titleSurname"/>
    <fmt:message key="registrationPage.title.email" var="titleEmail"/>
    <fmt:message key="registrationPage.placeholder.password" var="placeholderPassword"/>
    <fmt:message key="registrationPage.title.password" var="titlePassword"/>
    <fmt:message key="registrationPage.placeholder.passwordRepeat" var="placeholderPasswordRepeat"/>
    <fmt:message key="registrationPage.title.passwordRepeat" var="titlePasswordRepeat"/>
    <fmt:message key="registrationPage.title.checkBox" var="titleCheckBox"/>
    <fmt:message key="registrationPage.text.checkBox" var="textCheckBox"/>
    <fmt:message key="registrationPage.button" var="button"/>
    <fmt:message key="registrationPage.authorize.question" var="question"/>
    <fmt:message key="registrationPage.authorize.text" var="text"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${pageContext.request.contextPath}/css/authorization.css" rel="stylesheet">
</head>
<body>
<div id="root">
    <div id="regModal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <div class="auth-text registration">
                    ${pageScope.registration}
                </div>
                <span class="close" id="regClose">&times;</span>
            </div>
            <form class="modal-container register" method="POST"
                  action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="common_register">
                <div class="group">
                    <input id="name" required class="field" type="text" placeholder="${pageScope.placeholderName}"
                           name="name" title="${pageScope.titleName}" maxlength="45">
                </div>
                <div class="group">
                    <input id="surname" required class="field" type="text" placeholder="${pageScope.placeholderSurname}"
                           name="surname" title="${pageScope.titleSurname}" maxlength="45">
                </div>
                <div class="group" id="email">
                    <input id="userEmail" required class="field" type="email" placeholder="mail@example.com"
                           name="email" title="${pageScope.titleEmail}" maxlength="45">
                </div>
                <div class="group">
                    <input id="password" required class="field" type="password"
                           placeholder="${pageScope.placeholderPassword}" name="password"
                           title="${pageScope.titlePassword}" maxlength="45">
                </div>
                <div class="group">
                    <input id="passwordRepeat" required class="field" type="password"
                           placeholder="${pageScope.placeholderPasswordRepeat}"
                           title="${pageScope.titlePasswordRepeat}">
                </div>
                <div class="group">
                    <label><input class="check-box" required type="checkbox" name="confirmation" id="check-box"
                                  title="${pageScope.titleCheckBox}">${pageScope.textCheckBox}</label>
                </div>
                <div class="group">
                    <button id="submit-r" class="submit-button" type="submit" disabled>${pageScope.button}</button>
                </div>
                <div class="group">
                    <div class="authorisation-text" id="toLogin">${pageScope.question}
                        <span class="authorisation" onclick="return signWhat(this.parentNode)">
                            ${pageScope.text}
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>