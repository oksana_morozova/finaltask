<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="custom" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="userPage.button.logOut" var="logOut"/>
    <fmt:message key="studentPage.address" var="address"/>
    <fmt:message key="studentPage.teacher" var="teacher"/>
    <fmt:message key="studentPage.taskList" var="taskList"/>
    <fmt:message key="studentPage.name" var="name"/>
    <fmt:message key="studentPage.description" var="description"/>
    <fmt:message key="studentPage.status" var="status"/>
    <fmt:message key="studentPage.mark" var="mark"/>
    <fmt:message key="studentPage.changeButton" var="changeButton"/>
    <fmt:message key="studentPage.info" var="info"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${sessionScope.user.name} ${sessionScope.user.surname}</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png" type="image/png">
    <link href="${pageContext.request.contextPath}/css/user.css" rel="stylesheet">
    <ctg:page_security/>
</head>
<body>
<c:if test="${requestScope.message ne null}">
    <jsp:include page="../common/errorWindow.jsp"/>
</c:if>
<div id="root">
    <header id="header">
        <div class="container">
            <div class="user-name">${sessionScope.user.name} ${sessionScope.user.surname}</div>
            <form class="log-out" method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="common_log_out">
                <button class="log-out">${pageScope.logOut}</button>
            </form>
        </div>
    </header>
    <div class="home-page">
        <div class="user-container">
            <div class="name">${sessionScope.user.name} ${sessionScope.user.surname}</div>
            <div class="small-text">${sessionScope.user.email}</div>
            <c:choose>
                <c:when test="${sessionScope.userInfo ne null}">
                    <div class="info">
                            ${sessionScope.userInfo.trainingName}
                        <div class="small-text">
                                ${pageScope.address} ${sessionScope.userInfo.address} |
                                ${pageScope.teacher} ${sessionScope.userInfo.name}
                                ${sessionScope.userInfo.surname}
                        </div>
                    </div>
                    <div class="info">${pageScope.taskList}</div>
                    <div class="info">
                        <table class="tasks">
                            <tbody>
                            <tr class="table-head">
                                <td>${pageScope.name}</td>
                                <td>${pageScope.description}</td>
                                <td>${pageScope.status}</td>
                                <td>${pageScope.mark}</td>
                            </tr>
                            <c:forEach var="task" items="${sessionScope.userTasks}">
                                <tr>
                                    <td>${task.name}</td>
                                    <td>${task.description}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${task.taskStatus == \"in_process\"}">
                                                <form method="POST"
                                                      action="${pageContext.request.contextPath}/trainingcenter">
                                                    <input type="hidden" name="command"
                                                           value="student_change_task_status">
                                                    <input type="hidden" name="taskName" value="${task.name}">
                                                    <input type="hidden" name="id" value="${sessionScope.user.id}">
                                                    <input type="hidden" name="task" value="${task}">
                                                    <input type="hidden" name="taskStatus" value="DONE">
                                                    <button class="change-btn">${pageScope.changeButton}</button>
                                                </form>
                                            </c:when>
                                            <c:otherwise>
                                                ${task.taskStatus}
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>${task.score}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="info">${pageScope.info}</div>
                </c:otherwise>
            </c:choose>
        </div>
        <br>
    </div>
    <footer id="footer">
        <tag:footer/>
    </footer>
</div>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/errorWindow.js"></script>
</html>