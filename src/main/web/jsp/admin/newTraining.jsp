<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="newTraining" var="newTraining"/>
    <fmt:message key="newTraining.placeholderTrainingName" var="placeholderTrainingName"/>
    <fmt:message key="newTraining.titleTrainingName" var="titleTrainingName"/>
    <fmt:message key="newTraining.placeholderDescription" var="placeholderDescription"/>
    <fmt:message key="newTraining.titleDescription" var="titleDescription"/>
    <fmt:message key="newTraining.titleRecruitment" var="titleRecruitment"/>
    <fmt:message key="newTraining.underRecruitment" var="underRecruitment"/>
    <fmt:message key="newTraining.forthcomingRecruitment" var="forthcomingRecruitment"/>
    <fmt:message key="newTraining.closedRecruitment" var="closedRecruitment"/>
    <fmt:message key="newTraining.placeholderDuration" var="placeholderDuration"/>
    <fmt:message key="newTraining.titleDuration" var="titleDuration"/>
    <fmt:message key="newTraining.placeholderNumberOfStudents" var="placeholderNumberOfStudents"/>
    <fmt:message key="newTraining.titleNumberOfStudents" var="titleNumberOfStudents"/>
    <fmt:message key="newTraining.titleCity" var="titleCity"/>
    <fmt:message key="newTraining.placeholderStartDate" var="placeholderStartDate"/>
    <fmt:message key="newTraining.titleStartDate" var="titleStartDate"/>
    <fmt:message key="newTraining.placeholderContactEmail" var="placeholderContactEmail"/>
    <fmt:message key="newTraining.titleContactEmail" var="titleContactEmail"/>
    <fmt:message key="newTraining.button" var="button"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${pageContext.request.contextPath}/css/authorization.css" rel="stylesheet">
</head>
<body>
<div id="root">
    <div id="training-modal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <div class="auth-text registration">${pageScope.newTraining}</div>
                <span class="close" id="training-close">&times;</span>
            </div>
            <form class="modal-container register" method="POST"
                  action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="admin_add_new_training">
                <div class="group">
                    <input id="trainingName" required class="field" type="text"
                           placeholder="${pageScope.placeholderTrainingName}" name="name"
                           title="${pageScope.titleTrainingName}" maxlength="45">
                </div>
                <div class="group">
                    <input id="description" required class="field" type="text"
                           placeholder="${pageScope.placeholderDescription}" name="description"
                           title="${pageScope.titleDescription}" maxlength="500">
                </div>
                <div class="group">
                    <select id="recruitment" name="recruitment" clacc="field" title="${pageScope.titleRecruitment}">
                        <option value="UNDER_RECRUITMENT">${pageScope.underRecruitment}</option>
                        <option value="FORTHCOMING_RECRUITMENT">${pageScope.forthcomingRecruitment}</option>
                        <option value="CLOSED_RECRUITMENT">${pageScope.closedRecruitment}</option>
                    </select>
                </div>
                <div class="group">
                    <input id="duration" required class="field" type="text"
                           placeholder="${pageScope.placeholderDuration}" name="duration"
                           title="${pageScope.titleDuration}" maxlength="45">
                </div>
                <div class="group">
                    <input id="numberOfStudents" required class="field" type="number"
                           placeholder="${pageScope.placeholderNumberOfStudents}"
                           name="numberOfStudents" title="${pageScope.titleNumberOfStudents}" min="0">
                </div>
                <div class="group">
                    <select id="city" name="city" clacc="field" title="${pageScope.titleCity}">
                        <c:forEach var="city" items="${sessionScope.cities}">
                            <option value="${city.id}">${city.city}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="group">
                    <input id="startDate" required class="field" type="text"
                           placeholder="${pageScope.placeholderStartDate}"
                           name="startDate" title="${pageScope.titleStartDate}" maxlength="45">
                </div>
                <div class="group">
                    <input id="contactEmail" required class="field" type="text"
                           placeholder="${pageScope.placeholderContactEmail}"
                           name="email" title="${pageScope.titleContactEmail}" maxlength="45">
                </div>
                <div class="group">
                    <button id="submit-add-training" class="submit-button" type="submit" disabled>
                        ${pageScope.button}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>