<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="newTeacher" var="newTeacher"/>
    <fmt:message key="registrationPage.placeholder.name" var="placeholderName"/>
    <fmt:message key="registrationPage.title.name" var="titleName"/>
    <fmt:message key="registrationPage.placeholder.surname" var="placeholderSurname"/>
    <fmt:message key="registrationPage.title.surname" var="titleSurname"/>
    <fmt:message key="registrationPage.title.email" var="titleEmail"/>
    <fmt:message key="registrationPage.placeholder.password" var="placeholderPassword"/>
    <fmt:message key="registrationPage.title.password" var="titlePassword"/>
    <fmt:message key="newTeacher.button" var="button"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${pageContext.request.contextPath}/css/authorization.css" rel="stylesheet">
</head>
<body>
<div id="root">
    <div id="teacher-modal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <div class="auth-text registration">${pageScope.newTeacher}</div>
                <span class="close" id="teacher-close">&times;</span>
            </div>
            <form class="modal-container register" method="POST"
                  action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="admin_add_new_teacher">
                <div class="group">
                    <input id="name" required class="field" type="text" placeholder="${pageScope.placeholderName}"
                           name="name" title="${pageScope.titleName}" maxlength="45">
                </div>
                <div class="group">
                    <input id="surname" required class="field" type="text" placeholder="${pageScope.placeholderSurname}"
                           name="surname" title="${pageScope.titleSurname}" maxlength="45">
                </div>
                <div class="group" id="email">
                    <input id="userEmail" required class="field" type="email" placeholder="mail@example.com"
                           name="email" title="${pageScope.titleEmail}" maxlength="45">
                </div>
                <div class="group">
                    <input id="password" required class="field" type="text"
                           placeholder="${pageScope.placeholderPassword}" name="password"
                           title="${pageScope.titlePassword}" maxlength="45">
                </div>
                <div class="group">
                    <button id="submit-add-teacher" class="submit-button" type="submit" disabled>
                        ${pageScope.button}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>