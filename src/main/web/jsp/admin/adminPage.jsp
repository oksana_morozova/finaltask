<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="custom" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="userPage.button.logOut" var="logOut"/>
    <fmt:message key="adminPage.button.addTeacher" var="addTeacher"/>
    <fmt:message key="adminPage.button.deleteTeacher" var="deleteTeacher"/>
    <fmt:message key="adminPage.button.addTraining" var="addTraining"/>
    <fmt:message key="adminPage.button.deleteTraining" var="deleteTraining"/>
    <fmt:message key="adminPage.button.viewTeachers" var="viewTeachers"/>
    <fmt:message key="adminPage.button.viewTrainings" var="viewTrainings"/>
    <fmt:message key="adminPage.nameSurname" var="nameSurname"/>
    <fmt:message key="adminPage.email" var="email"/>
    <fmt:message key="adminPage.password" var="password"/>
    <fmt:message key="adminPage.option" var="option"/>
    <fmt:message key="adminPage.trainingName" var="trainingName"/>
    <fmt:message key="adminPage.trainingCityAndId" var="trainingCityAndId"/>
    <fmt:message key="adminPage.trainingStartDate" var="trainingStartDate"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${sessionScope.user.name} ${sessionScope.user.surname}</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png" type="image/png">
    <link href="${pageContext.request.contextPath}/css/user.css" rel="stylesheet">
    <ctg:page_security/>
</head>
<body>
<c:if test="${requestScope.message ne null}">
    <jsp:include page="../common/errorWindow.jsp"/>
</c:if>
<div id="root">
    <header id="header">
        <div class="container">
            <div class="user-name">${sessionScope.user.name} ${sessionScope.user.surname}</div>
            <form class="log-out" method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="common_log_out">
                <button class="log-out">${pageScope.logOut}</button>
            </form>
        </div>
    </header>
    <div class="home-page">
        <jsp:include page="newTeacher.jsp"/>
        <jsp:include page="newTraining.jsp"/>
        <div class="user-container">
            <div class="name">${sessionScope.user.name} ${sessionScope.user.surname}</div>
            <div class="small-text">${sessionScope.user.email}</div>
            <div>
                <button id="add-teacher" class="change-btn admin">${pageScope.addTeacher}</button>
                <c:if test="${sessionScope.teachers ne null}">
                    <div class="info">
                        <table class="tasks">
                            <tbody>
                            <tr class="table-head">
                                <td>${pageScope.nameSurname}</td>
                                <td>${pageScope.email}</td>
                                <td>${pageScope.option}</td>
                            </tr>
                            <c:forEach var="teacher" items="${sessionScope.teachers}">
                                <tr>
                                    <td>${teacher.name} ${teacher.surname}</td>
                                    <td>${teacher.email}</td>
                                    <td>
                                        <form method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                                            <input type="hidden" name="command" value="admin_delete_teacher">
                                            <input type="hidden" name="id" value="${teacher.id}">
                                            <button class="change-btn admin" type="submit">
                                                    ${pageScope.deleteTeacher}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>
            </div>
            <div>
                <button id="add-training" class="change-btn admin">${pageScope.addTraining}</button>
                <c:if test="${sessionScope.trainings ne null}">
                    <div class="info">
                        <table class="tasks">
                            <tbody>
                            <tr class="table-head">
                                <td>${pageScope.trainingName}</td>
                                <td>${pageScope.trainingCityAndId}</td>
                                <td>${pageScope.trainingStartDate}</td>
                                <td>${pageScope.option}</td>
                            </tr>
                            <c:forEach var="training" items="${sessionScope.trainings}">
                                <tr>
                                    <td>${training.name}</td>
                                    <td>
                                            ${training.cityId}
                                        <c:forEach var="city" items="${sessionScope.cities}">
                                            <c:if test="${training.cityId eq city.id}">
                                                (${city.city})
                                            </c:if>
                                        </c:forEach>
                                    </td>
                                    <td> ${training.startDate}</td>
                                    <td>
                                        <form method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                                            <input type="hidden" name="command" value="admin_delete_training">
                                            <input type="hidden" name="id" value="${training.id}">
                                            <button class="change-btn admin" type="submit">
                                                    ${pageScope.deleteTraining}
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:if>
            </div>
        </div>
        <br>
    </div>
    <footer id="footer">
        <tag:footer/>
    </footer>
</div>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/adminPage.js"></script>
<script src="${pageContext.request.contextPath}/js/errorWindow.js"></script>
</html>