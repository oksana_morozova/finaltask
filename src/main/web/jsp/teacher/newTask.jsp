<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="newTaskPage.newTask" var="newTask"/>
    <fmt:message key="newTaskPage.placeholder.taskName" var="placeholderTaskName"/>
    <fmt:message key="newTaskPage.title.taskName" var="titleTaskName"/>
    <fmt:message key="newTaskPage.placeholder.description" var="placeholderDescription"/>
    <fmt:message key="newTaskPage.title.description" var="titleDescription"/>
    <fmt:message key="newTaskPage.button" var="button"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="${pageContext.request.contextPath}/css/authorization.css" rel="stylesheet">
</head>
<body>
<div id="root">
    <div id="modal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <div class="auth-text registration">${pageScope.newTask}</div>
                <span class="close" id="close">&times;</span>
            </div>
            <form class="modal-container register" method="POST"
                  action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="teacher_add_new_task">
                <input type="hidden" name="id" value="${sessionScope.teacherTasks[0].id}">
                <input type="hidden" name="userId" value="${sessionScope.user.id}">
                <div class="group">
                    <input id="taskName" required class="field" type="text"
                           placeholder="${pageScope.placeholderTaskName}" name="taskName"
                           title="${pageScope.titleTaskName}" maxlength="45">
                </div>
                <div class="group">
                    <input id="description" required class="field" type="text"
                           placeholder=" ${pageScope.placeholderDescription}" name="description"
                           title=" ${pageScope.titleDescription}" maxlength="500">
                </div>
                <div class="group">
                    <button id="submit-r" class="submit-button" type="submit" disabled> ${pageScope.button}</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>