<%@ page contentType="text/html; charset=UTF-8" language="JAVA" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="custom" %>
<fmt:requestEncoding value="utf-8"/>

<fmt:bundle basename="trainingCenter">
    <fmt:message key="userPage.button.logOut" var="logOut"/>
    <fmt:message key="teacherPage.students" var="students"/>
    <fmt:message key="teacherPage.addButton" var="addButton"/>
    <fmt:message key="teacherPage.nameSurname" var="nameSurname"/>
    <fmt:message key="teacherPage.groupTraining" var="groupTraining"/>
    <fmt:message key="teacherPage.taskName" var="taskName"/>
    <fmt:message key="teacherPage.taskStatus" var="taskStatus"/>
    <fmt:message key="teacherPage.mark" var="mark"/>
    <fmt:message key="teacherPage.setButton" var="setButton"/>
    <fmt:message key="teacherPage.info" var="info"/>
</fmt:bundle>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>${sessionScope.user.name} ${sessionScope.user.surname}</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png" type="image/png">
    <link href="${pageContext.request.contextPath}/css/user.css" rel="stylesheet">
    <ctg:page_security/>
</head>
<body>
<c:if test="${requestScope.message ne null}">
    <jsp:include page="../common/errorWindow.jsp"/>
</c:if>
<div id="root">
    <header id="header">
        <div class="container">
            <div class="user-name">${sessionScope.user.name} ${sessionScope.user.surname}</div>
            <form class="log-out" method="POST" action="${pageContext.request.contextPath}/trainingcenter">
                <input type="hidden" name="command" value="common_log_out">
                <button class="log-out">${pageScope.logOut}</button>
            </form>
        </div>
    </header>
    <div class="home-page">
        <div class="user-container">
            <div class="name">${sessionScope.user.name} ${sessionScope.user.surname}</div>
            <div class="small-text">${sessionScope.user.email}</div>
            <c:choose>
                <c:when test="${sessionScope.teacherTasks ne null}">
                    <div class="info">
                            ${pageScope.students}
                        <button id="new-task" class="change-btn add">${pageScope.addButton}</button>
                        <jsp:include page="newTask.jsp"/>
                    </div>
                    <div class="info">
                        <table class="tasks">
                            <tbody>
                            <tr class="table-head">
                                <td>${pageScope.nameSurname}</td>
                                <td>${pageScope.groupTraining}</td>
                                <td>${pageScope.taskName}</td>
                                <td>${pageScope.taskStatus}</td>
                                <td>${pageScope.mark}</td>
                            </tr>
                            <c:forEach var="task" items="${sessionScope.teacherTasks}">
                                <tr>
                                    <td>${task.name} ${task.surname}</td>
                                    <td>
                                            ${task.id}
                                        (${task.trainingName})
                                    </td>
                                    <td> ${task.taskName}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${task.taskStatus == \"in_process\"}">
                                                in process
                                            </c:when>
                                            <c:otherwise>
                                                ${task.taskStatus}
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${task.taskStatus == \"done\"}">
                                                <form method="POST"
                                                      action="${pageContext.request.contextPath}/trainingcenter">
                                                    <input type="hidden" name="command" value="teacher_set_mark">
                                                    <input type="hidden" name="taskName"
                                                           value="${task.taskName}">
                                                    <input type="hidden" name="id"
                                                           value="${task.studentId}">
                                                    <input type="hidden" name="task"
                                                           value="${sessionScope.teacherTasks}">
                                                    <input type="hidden" name="userId"
                                                           value="${sessionScope.user.id}">
                                                    <input class="mark-input" type="number" min="0" max="10"
                                                           name="mark">
                                                    <button class="change-btn">${pageScope.setButton}</button>
                                                </form>
                                            </c:when>
                                            <c:otherwise>
                                                ${task.mark}
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="info">${pageScope.info}</div>
                </c:otherwise>
            </c:choose>
        </div>
        <br>
    </div>
    <footer id="footer">
        <tag:footer/>
    </footer>
</div>
</div>
</body>
<script src="${pageContext.request.contextPath}/js/teacherPage.js"></script>
<script src="${pageContext.request.contextPath}/js/errorWindow.js"></script>
</html>